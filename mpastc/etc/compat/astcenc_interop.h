// SPDX-License-Identifier: Apache-2.0
// ----------------------------------------------------------------------------
// Copyright 2011-2023 Arm Limited
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not
// use this file except in compliance with the License. You may obtain a copy
// of the License at:
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
// WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
// License for the specific language governing permissions and limitations
// under the License.
// ----------------------------------------------------------------------------

/* These functions provide the bare minimum structures, constants, etc. of
 * astcenc in pure C so all components of mpastc can interoperate. */

#ifndef MPASTC_ETC_ASTCENC_INTEROP_H_
#define MPASTC_ETC_ASTCENC_INTEROP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

/**
 * @brief The ASTC quantization methods.
 *
 * Note, the values here are used directly in the encoding in the format so do
 * not rearrange.
 */
typedef enum quant_method {
    QUANT_2 = 0,
    QUANT_3 = 1,
    QUANT_4 = 2,
    QUANT_5 = 3,
    QUANT_6 = 4,
    QUANT_8 = 5,
    QUANT_10 = 6,
    QUANT_12 = 7,
    QUANT_16 = 8,
    QUANT_20 = 9,
    QUANT_24 = 10,
    QUANT_32 = 11,
    QUANT_40 = 12,
    QUANT_48 = 13,
    QUANT_64 = 14,
    QUANT_80 = 15,
    QUANT_96 = 16,
    QUANT_128 = 17,
    QUANT_160 = 18,
    QUANT_192 = 19,
    QUANT_256 = 20,
    QUANT_MAX
} quant_method;

/**
 * @brief The number of levels use by an ASTC quantization method.
 *
 * @param method   The quantization method
 *
 * @return   The number of levels used by @c method.
 */
static inline unsigned int get_quant_level(quant_method method) {
    switch (method) {
    case QUANT_2:
        return 2;
    case QUANT_3:
        return 3;
    case QUANT_4:
        return 4;
    case QUANT_5:
        return 5;
    case QUANT_6:
        return 6;
    case QUANT_8:
        return 8;
    case QUANT_10:
        return 10;
    case QUANT_12:
        return 12;
    case QUANT_16:
        return 16;
    case QUANT_20:
        return 20;
    case QUANT_24:
        return 24;
    case QUANT_32:
        return 32;
    case QUANT_40:
        return 40;
    case QUANT_48:
        return 48;
    case QUANT_64:
        return 64;
    case QUANT_80:
        return 80;
    case QUANT_96:
        return 96;
    case QUANT_128:
        return 128;
    case QUANT_160:
        return 160;
    case QUANT_192:
        return 192;
    case QUANT_256:
        return 256;
    case QUANT_MAX:
        return 0;
    }

    // Unreachable - the enum is fully described
    return 0;
}

static inline const char * get_quant_level_string(quant_method method) {
    switch (method) {
    case QUANT_2:
        return "QUANT_2";
    case QUANT_3:
        return "QUANT_3";
    case QUANT_4:
        return "QUANT_4";
    case QUANT_5:
        return "QUANT_5";
    case QUANT_6:
        return "QUANT_6";
    case QUANT_8:
        return "QUANT_8";
    case QUANT_10:
        return "QUANT_10";
    case QUANT_12:
        return "QUANT_12";
    case QUANT_16:
        return "QUANT_16";
    case QUANT_20:
        return "QUANT_20";
    case QUANT_24:
        return "QUANT_24";
    case QUANT_32:
        return "QUANT_32";
    case QUANT_40:
        return "QUANT_40";
    case QUANT_48:
        return "QUANT_48";
    case QUANT_64:
        return "QUANT_64";
    case QUANT_80:
        return "QUANT_80";
    case QUANT_96:
        return "QUANT_96";
    case QUANT_128:
        return "QUANT_128";
    case QUANT_160:
        return "QUANT_160";
    case QUANT_192:
        return "QUANT_192";
    case QUANT_256:
        return "QUANT_256";
    case QUANT_MAX:
        return 0;
    }

    return 0;
}

#ifdef __cplusplus
}
#endif

#endif