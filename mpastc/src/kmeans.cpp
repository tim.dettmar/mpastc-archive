#include "kmeans.h"

int is_chosen(int8_t * chosen, uint8_t k, uint8_t idx) {
    for (int i = 0; i < k; i++) {
        if (chosen[i] == idx)
            return 1;
    }
    return 0;
}

int kmeans_pp(ir_texel * texels, int8_t k, uint8_t bs, uint32_t max_iter,
              uint8_t * clusters) {
    assert(k < bs);
    int8_t chosen[k];
    ir_texel centroids[k];
    std::random_device r;
    std::mt19937 mt(r());
    for (int i = 0; i < k; i++) {
        chosen[i] = -1;
    }

    {
        uint8_t idx = mt() % bs;
        centroids[0] = texels[idx];
        chosen[0] = idx;
    }

    for (int i = 1; i < k; i++) {
        std::vector<uint32_t> dist(bs);
        for (int j = 0; j < bs; j++) {
            if (!is_chosen(chosen, k, j)) {
                /* Use the minimum distance of all current selected centroids in
                 * the calculation */
                ir_fmt min_dist = INFINITY;
                for (int h = i - 1; h >= 0; h--) {
                    ir_fmt dist = ir_texel_euc_dist(centroids[h], texels[j]);
                    dist = dist * dist;
                    if (dist < min_dist) {
                        min_dist = dist;
                    }
                    if (min_dist == 0) {
                        break;
                    }
                }
                /* Multiply to preserve some fractional accuracy. The actual
                 * calculated squared distance ranges from 0 - 195K in RGB space
                 */
                dist[j] = (uint32_t)(min_dist * 100.0f);
            } else {
                dist[j] = 0;
            }
        }
        std::discrete_distribution<uint32_t> d(dist.begin(), dist.end());
        uint8_t idx = d(mt);
        centroids[i] = texels[idx];
        chosen[i] = idx;
    }

    return kmeans(texels, centroids, k, bs, max_iter, clusters);
}

int kmeans(ir_texel * texels, ir_texel * init, int8_t k, uint8_t bs,
           uint32_t max_iter, uint8_t * clusters) {
    if (!texels || !clusters || k < 2 || k >= bs)
        return -EINVAL;

    ir_texel cent_tmp[k];
    if (!init) {
        uint8_t used[bs];
        memset(used, 0, k);
        for (uint8_t i = 0; i < k; i++) {
            /* Select a random position for initial centroids */
            while (1) {
                cent_tmp[i].r = random() % 256;
                cent_tmp[i].g = random() % 256;
                cent_tmp[i].b = random() % 256;
                break;
            }
        }
    } else {
        memcpy(cent_tmp, init, sizeof(ir_texel) * k);
    }

    for (uint8_t i = 0; i < bs; i++) {
        clusters[i] = 255;
    }

    uint32_t iter = 0;
    while (1) {
        uint8_t changed = 0;
        for (uint8_t i = 0; i < bs; i++) {
            ir_fmt min_dist = ir_type_max;
            int8_t c = -1;
            for (uint8_t j = 0; j < k; j++) {
                ir_fmt dist = ir_texel_euc_dist(cent_tmp[j], texels[i]);
                if (dist < 0.1f) {
                    c = j;
                    break;
                }
                if (dist < min_dist) {
                    min_dist = dist;
                    c = j;
                }
            }
            assert(c >= 0);
            if (clusters[i] != c) {
                changed = 1;
                clusters[i] = c;
            }
        }

        if (!changed)
            return 0;

        /* Recalculate centroids */
        uint8_t same = 0;
        for (uint8_t i = 0; i < k; i++) {
            ir_texel prev_cent = cent_tmp[i];
            cent_tmp[i].r = ir_zero;
            cent_tmp[i].g = ir_zero;
            cent_tmp[i].b = ir_zero;
            uint8_t n = 0;
            for (uint8_t j = 0; j < bs; j++) {
                if (clusters[j] == i) {
                    cent_tmp[i] = ir_texel_addv(cent_tmp[i], texels[j]);
                    n++;
                }
            }
            ir_endpoint diff;
            diff.a = cent_tmp[i];
            diff.b = prev_cent;
            cent_tmp[i] = ir_texel_div(cent_tmp[i], n);
            if (ir_texel_magnitude(ir_ep_vectorize(diff)) < 0.1f)
                same++;
        }

        if (same == k)
            return 0;

        iter++;

        if (iter > max_iter)
            return 1;
    }
}