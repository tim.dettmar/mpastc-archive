#include "encode.h"
#include "bise_tables.h"
#include "bitwriter.h"

/* 23.8 Weight Range Encodings: P0 P1 P2 P
 * Bit configurations for all supported ranges */
const uint8_t range_table[][4] = {
    /* Low-precision range */
    {0, 1, 0, 0},
    {1, 1, 0, 0},
    {0, 0, 1, 0},
    {1, 0, 1, 0},
    {0, 1, 1, 0},
    {1, 1, 1, 0},
    /* High-precision range */
    {0, 1, 0, 1},
    {1, 1, 0, 1},
    {0, 0, 1, 1},
    {1, 0, 1, 1},
    {0, 1, 1, 1},
    {1, 1, 1, 1},
    {255, 255, 255, 255}};

uint16_t get_block_mode(quant_method wt_quant) {
    uint16_t p0 = range_table[wt_quant][0];
    uint16_t p1 = range_table[wt_quant][1];
    uint16_t p2 = range_table[wt_quant][2];
    uint16_t p = range_table[wt_quant][3];
    uint16_t w = 4;
    uint16_t h = 4;
    return (p1 | (p2 << 1) | (p0 << 4) | ((h - 2) << 5) | ((w - 4) << 7)) &
           0b01111110011;
}

// void encode_astc_block(ir_endpoint * eps, ir_weight * wts, uint8_t parts,
//                        uint16_t part_index, quant_method weight_range,
//                        quant_method colour_range, uint8_t * out) {
//     symbolic_compressed_block scb;
//     scb.block_mode = get_block_mode(weight_range);
//     scb.block_type = SYM_BTYPE_NONCONST;
//     for (uint8_t i = 0; i < parts; i++) {
//         scb.color_formats[i] = 8; // CEM_LDR_RGB_DIRECT
//         scb.color_values[i][0] = (uint8_t)eps->a.r;
//         scb.color_values[i][1] = (uint8_t)eps->b.r;
//         scb.color_values[i][2] = (uint8_t)eps->a.g;
//         scb.color_values[i][3] = (uint8_t)eps->b.g;
//         scb.color_values[i][4] = (uint8_t)eps->a.b;
//         scb.color_values[i][5] = (uint8_t)eps->b.b;
//         scb.color_values[i][6] = (uint8_t)255;
//         scb.color_values[i][7] = (uint8_t)255;
//         // scb.color_values[i][0] = 255;
//         // scb.color_values[i][1] = 255;
//         // scb.color_values[i][2] = 255;
//         // scb.color_values[i][3] = 255;
//         // scb.color_values[i][4] = 255;
//         // scb.color_values[i][5] = 255;
//         // scb.color_values[i][6] = 255;
//         // scb.color_values[i][7] = 255;
//     }
//     scb.color_formats_matched = 1;
//     scb.quant_mode = colour_range;
//     scb.partition_count = parts;
//     scb.partition_index = part_index;
//     for (uint8_t i = 0; i < 16; i++) {
//         scb.weights[i] = static_cast<uint8_t>(
//             astc::max(astc::min(wts[i].weight * 64.0f, 64.0f), 0.0f));
//     }
//     physical_compressed_block pcb;
//     symbolic_to_physical(scb, pcb, weight_range);
//     for (uint8_t i = 0; i < 16; i++) {
//         out[i] = pcb.data[i];
//     }
// }

/* Cumulative number of bits required to store n trits */
const uint8_t trits_bitcount[] = {0, 2, 4, 5, 7, 8};

/* Cumulative number of bits required to store n quints */
const uint8_t quints_bitcount[] = {0, 3, 5, 7};

/* 23.20, most efficient bit/trit/quint combination for given range. From
 * astcrt. */
const uint8_t bits_trits_quints_table[QUANT_MAX][3] = {
    {1, 0, 0}, // RANGE_2
    {0, 1, 0}, // RANGE_3
    {2, 0, 0}, // RANGE_4
    {0, 0, 1}, // RANGE_5
    {1, 1, 0}, // RANGE_6
    {3, 0, 0}, // RANGE_8
    {1, 0, 1}, // RANGE_10
    {2, 1, 0}, // RANGE_12
    {4, 0, 0}, // RANGE_16
    {2, 0, 1}, // RANGE_20
    {3, 1, 0}, // RANGE_24
    {5, 0, 0}, // RANGE_32
    {3, 0, 1}, // RANGE_40
    {4, 1, 0}, // RANGE_48
    {6, 0, 0}, // RANGE_64
    {4, 0, 1}, // RANGE_80
    {5, 1, 0}, // RANGE_96
    {7, 0, 0}, // RANGE_128
    {5, 0, 1}, // RANGE_160
    {6, 1, 0}, // RANGE_192
    {8, 0, 0}  // RANGE_256
};

uint8_t reverse_byte(uint8_t b) {
    b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
    b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
    b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
    return b;
}

void reverse_bit_order(uint8_t * buf, uint8_t * out, uint64_t nb) {
    uint8_t tmp = 0;
    uint64_t j = nb - 1;
    for (uint64_t i = 0; i < nb; i++) {
        out[j--] = reverse_byte(buf[i]);
    }
}

/* Get the number of bits required to store an LDR RGB direct encoded block */
uint64_t get_bitcount(uint8_t parts, uint8_t n_wts, quant_method wt_range,
                      quant_method col_range, uint8_t * hdr, uint8_t * wt,
                      uint8_t * col) {
    uint8_t header_bits = parts > 1 ? 29 : 17;

    uint8_t wt_bits = bits_trits_quints_table[wt_range][0];
    uint8_t wt_trits = bits_trits_quints_table[wt_range][1];
    uint8_t wt_quints = bits_trits_quints_table[wt_range][2];

    uint8_t col_bits = bits_trits_quints_table[col_range][0];
    uint8_t col_trits = bits_trits_quints_table[col_range][1];
    uint8_t col_quints = bits_trits_quints_table[col_range][2];

    uint16_t wt_total = wt_bits * n_wts;
    if (wt_trits)
        wt_total += (n_wts / 5 * 8 + trits_bitcount[n_wts % 5]);
    if (wt_quints)
        wt_total += (n_wts / 3 * 7 + quints_bitcount[n_wts % 3]);
    if (wt)
        *wt = wt_total;

    uint16_t n_channels = 6 * parts;
    uint16_t col_total = col_bits * n_channels;
    if (col_trits)
        col_total += (n_channels / 5 * 8 + trits_bitcount[n_channels % 5]);
    if (col_quints)
        col_total += (n_channels / 3 * 7 + quints_bitcount[n_channels % 3]);
    if (col)
        *col = col_total;
    if (hdr)
        *hdr = header_bits;

    return header_bits + wt_total + col_total;
}

#define btwn(var, min, max) (var >= min && var < max)

void encode_void_extent(ir_texel colour, void * out) {
    (*(uint64_t *)out) = 0xFFFFFFFFFFFFFDFC;
    uint16_t * out_tmp = (uint16_t *)(((uint64_t *)out) + 1);
    /* Scale from 0-255 to unorm16 0 - 65535 */
    out_tmp[0] = (uint16_t)(colour.r * 256.216f);
    out_tmp[1] = (uint16_t)(colour.g * 256.216f);
    out_tmp[2] = (uint16_t)(colour.b * 256.216f);
    out_tmp[3] = (uint16_t)-1;
}

uint64_t encode_rgb_endpoints(ir_endpoint * ep, uint8_t parts, quant_method r,
                              uint64_t offset, uint8_t * out) {
    uint64_t tmp_off = offset;
    texel aa, bb;
    uint8_t buf[6 * parts];
    memset(buf, 0, 6 * parts);
    for (uint8_t p = 0; p < parts; p++) {
        if (ir_texel_sum(ep[p].a) > ir_texel_sum(ep[p].b)) {
            assert(false && "Texel order error");
        } else {
            aa = ir_downsample_rgb(ep[p].a, r);
            bb = ir_downsample_rgb(ep[p].b, r);
        }
        uint8_t * buf_base = &buf[6 * p];
        buf_base[0] = aa.r;
        buf_base[1] = bb.r;
        buf_base[2] = aa.g;
        buf_base[3] = bb.g;
        buf_base[4] = aa.b;
        buf_base[5] = bb.b;
    }
    tmp_off += encode_bise(6 * parts, tmp_off, r, buf, out);
    return tmp_off - offset;
}

uint64_t encode_weights(ir_weight * wts, ir_endpoint * eps, quant_method r,
                        uint8_t * block_base) {
    uint8_t sbuf[16];
    uint8_t dbuf[16];
    uint16_t mul = get_quant_level(r) - 1;
    for (int i = 0; i < 16; i++) {
        ir_fmt norm_wt = wts[i].weight * mul;
        if (ir_mod(norm_wt, 1) > 0.5f)
            norm_wt += 0.6f;
        sbuf[i] = (uint8_t) norm_wt;
    }
    /* The bits are stored in reverse order, starting from the MSB. */
    uint64_t bits = encode_bise(16, 0, r, sbuf, dbuf);
    int i = 15;
    int j = 0;
    uint64_t ret = bits;
    while (bits) {
        if (bits >= 8)
            block_base[i--] = reverse_byte(dbuf[j++]);
        else {
            uint8_t lo_bits = (1 << (8 - bits)) - 1;
            block_base[i] = (block_base[i] & lo_bits) |
                            (reverse_byte(dbuf[j]) & (~lo_bits));
            break;
        }
        bits -= 8;
    }
    return ret;
}

uint8_t encode_trits(uint8_t low_bits, uint8_t n, uint64_t offset,
                     uint8_t * vals, uint8_t * out) {

    assert(n <= 5);
    uint8_t lo[5] = {0, 0, 0, 0, 0};
    uint8_t hi[5] = {0, 0, 0, 0, 0};
    for (int i = 0; i < n; i++) {
        lo[i] = vals[i] & ((1 << low_bits) - 1);
        hi[i] = vals[i] >> low_bits;
        assert(hi[i] < 3);
    }

    uint8_t packed = integer_of_trits[hi[4]][hi[3]][hi[2]][hi[1]][hi[0]];
    uint64_t pos = offset;

    writebits(&lo[0], out, pos, low_bits);
    pos += low_bits;
    writebits(&packed, out, pos, 2);
    packed >>= 2;
    pos += 2;
    if (n > 1) {
        writebits(&lo[1], out, pos, low_bits);
        pos += low_bits;
        writebits(&packed, out, pos, 2);
        packed >>= 2;
        pos += 2;
    }
    if (n > 2) {
        writebits(&lo[2], out, pos, low_bits);
        pos += low_bits;
        writebits(&packed, out, pos, 1);
        packed >>= 1;
        pos += 1;
    }
    if (n > 3) {
        writebits(&lo[3], out, pos, low_bits);
        pos += low_bits;
        writebits(&packed, out, pos, 2);
        packed >>= 2;
        pos += 2;
    }
    if (n > 4) {
        writebits(&lo[4], out, pos, low_bits);
        pos += low_bits;
        writebits(&packed, out, pos, 1);
        pos += 1;
    }

    assert(pos > offset);
    return pos - offset;
}

uint8_t encode_quints(uint8_t low_bits, uint8_t n, uint64_t offset,
                      uint8_t * vals, uint8_t * out) {
    assert(n <= 3);
    uint8_t lo[3] = {0, 0, 0};
    uint8_t hi[3] = {0, 0, 0};
    for (int i = 0; i < n; i++) {
        lo[i] = vals[i] & ((1 << low_bits) - 1);
        hi[i] = vals[i] >> low_bits;
        assert(hi[i] < 5);
    }

    uint8_t packed = integer_of_quints[hi[2]][hi[1]][hi[0]];
    uint64_t pos = offset;

    writebits(&lo[0], out, pos, low_bits);
    pos += low_bits;
    writebits(&packed, out, pos, 3);
    packed >>= 3;
    pos += 3;
    if (n > 1) {
        writebits(&lo[1], out, pos, low_bits);
        pos += low_bits;
        writebits(&packed, out, pos, 2);
        packed >>= 2;
        pos += 2;
    }
    if (n > 2) {
        writebits(&lo[2], out, pos, low_bits);
        pos += low_bits;
        writebits(&packed, out, pos, 2);
        pos += 2;
    }

    assert(pos > offset);
    return pos - offset;
}

uint64_t encode_trits_multi(uint8_t low_bits, uint64_t n, uint64_t offset,
                            uint8_t * vals, uint8_t * out) {
    uint64_t cur_offset = offset;
    while (n) {
        uint8_t blk_n = n > 5 ? 5 : n;
        uint8_t written = encode_trits(low_bits, blk_n, cur_offset, vals, out);
        cur_offset += written;
        vals += blk_n;
        n -= blk_n;
    }
    assert(cur_offset > offset);
    return cur_offset - offset;
}

uint64_t encode_quints_multi(uint8_t low_bits, uint64_t n, uint64_t offset,
                             uint8_t * vals, uint8_t * out) {
    uint64_t cur_offset = offset;
    while (n) {
        uint8_t blk_n = n > 3 ? 3 : n;
        uint8_t written = encode_quints(low_bits, blk_n, cur_offset, vals, out);
        cur_offset += written;
        vals += blk_n;
        n -= blk_n;
    }
    return cur_offset - offset;
}

uint64_t encode_bits_multi(uint8_t bits, uint64_t n, uint64_t offset,
                           uint8_t * vals, uint8_t * out) {
    for (uint64_t i = 0; i < n; i++) {
        writebits(&vals[i], out, offset + bits * i, bits);
    }
    return bits * n;
}

uint64_t encode_bise(uint64_t count, uint64_t offset, quant_method range,
                     uint8_t * vals, uint8_t * out) {
    if (range < 0 || range > QUANT_MAX)
        return 0;

    uint8_t bits = bits_trits_quints_table[range][0];
    uint8_t trits = bits_trits_quints_table[range][1];
    uint8_t quints = bits_trits_quints_table[range][2];

#ifndef NDEBUG
    unsigned int max_val = get_quant_level(range);
    for (uint64_t i = 0; i < count; i++) {
        assert(vals[i] < max_val);
    }
#endif

    if (trits)
        return encode_trits_multi(bits, count, offset, vals, out);
    else if (quints)
        return encode_quints_multi(bits, count, offset, vals, out);
    else
        return encode_bits_multi(bits, count, offset, vals, out);
}

uint64_t calc_bitcount(quant_method r, uint32_t n_values) {
    uint8_t bits = bits_trits_quints_table[r][0];
    uint8_t trits = bits_trits_quints_table[r][1];
    uint8_t quints = bits_trits_quints_table[r][2];

    uint64_t bitcount = 0;
    if (bits)
        bitcount += bits * n_values;
    if (trits)
        bitcount += n_values / 5 * 8 + trits_bitcount[n_values % 5];
    if (quints)
        bitcount += n_values / 3 * 7 + quints_bitcount[n_values % 3];
    return bitcount;
}

int set_block_header(uint8_t w, uint8_t h, uint8_t dp, uint8_t parts,
                     uint16_t part_index, quant_method wt_range,
                     uint64_t offset, uint8_t * out) {
    if (parts < 1 || parts > 4)
        return -EINVAL;

    if (wt_range >= QUANT_MAX)
        return -EINVAL;

    uint8_t zero = 0;
    uint8_t cem = CEM_LDR_RGB_DIRECT; // Fixed value
    uint8_t cem_ep = 0;               // Same CEM for all partitions
    uint8_t p0 = range_table[wt_range][0];
    uint8_t p1 = range_table[wt_range][1];
    uint8_t p2 = range_table[wt_range][2];
    uint8_t p = range_table[wt_range][3];

    /* Write according to Table 23.9, the first row is for 4x4 blocks */
    writebits(&p1, out, offset++, 1);
    writebits(&p2, out, offset++, 1);
    writebits(&zero, out, offset, 2);
    offset += 2;
    writebits(&p0, out, offset++, 1);
    h -= 2;
    w -= 4;
    writebits(&h, out, offset, 2);
    offset += 2;
    writebits(&w, out, offset, 2);
    offset += 2;
    writebits(&p, out, offset++, 1);
    writebits(&zero, out, offset++, 1);

    /* 23.9 The “Part” field specifies the number of partitions, minus one */
    parts--;
    writebits(&parts, out, offset, 2);
    offset += 2;
    if (parts) {
        writebits((uint8_t *)&part_index, out, offset, 10); // Partition index
        offset += 10;
        /* 23.12 CEM encoding. does not work when cem_ep != 0 */
        cem = (cem << 2) & 0b111100;
        writebits(&cem, out, offset, 6);
        offset += 6;
    } else {
        writebits(&cem, out, offset, 4);
        offset += 4;
    }
    return offset;
}