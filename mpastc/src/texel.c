#include "texel.h"

void ir_texel_decode(ir_endpoint * eps, ir_weight * weights, uint8_t bs,
                     texel * out) {
    uint8_t part_used[4] = {0, 0, 0, 0};
    for (int i = 0; i < bs; i++) {
        check_valid_weight(&weights[i]);
        part_used[weights[i].part] = 1;
    }
    uint8_t flag = 0;
    for (int i = 0; i < 4; i++) {
        if (part_used[i])
            check_valid_ep(&eps[i]);
    }
    for (int i = 0; i < bs; i++) {
        out[i] = ir_downsample_rgb(
            ir_texel_interp(eps[weights[i].part], weights[i].weight),
            QUANT_256);
    }
}

void ir_texel_quantize(ir_endpoint * eps, ir_weight * weights, uint8_t bs,
                       uint8_t parts, ir_fmt c_delta, ir_fmt w_delta,
                       ir_texel * quantized) {
    ir_endpoint tmp_eps[parts];
    ir_weight tmp_wts[bs];
    uint8_t part_used[4] = {0, 0, 0, 0};
    for (int i = 0; i < bs; i++) {
        check_valid_weight(&weights[i]);
        tmp_wts[i].weight = ir_quantize_value(weights[i].weight, w_delta);
        ir_clamp(weights[i].weight, 0.0f, 1.0f);
        tmp_wts[i].part = weights[i].part;
        part_used[tmp_wts[i].part] = 1;
    }
#ifndef NDEBUG
    uint8_t flag = 0;
#endif
    for (int i = 0; i < parts; i++) {
        if (!part_used[i])
            continue;

        check_valid_ep(&eps[i]);
        tmp_eps[i].a.r = ir_quantize_value(eps[i].a.r, c_delta);
        tmp_eps[i].a.g = ir_quantize_value(eps[i].a.g, c_delta);
        tmp_eps[i].a.b = ir_quantize_value(eps[i].a.b, c_delta);
        tmp_eps[i].b.r = ir_quantize_value(eps[i].b.r, c_delta);
        tmp_eps[i].b.g = ir_quantize_value(eps[i].b.g, c_delta);
        tmp_eps[i].b.b = ir_quantize_value(eps[i].b.b, c_delta);
        check_valid_ep(&tmp_eps[i]);
#ifndef NDEBUG
        flag = 1;
#endif
    }
#ifndef NDEBUG
    assert(flag);
#endif
    for (int i = 0; i < bs; i++) {
        quantized[i] =
            ir_texel_interp(tmp_eps[tmp_wts[i].part], tmp_wts[i].weight);
    }
}