#include "part.h"
#include "partition.h"

#ifdef BYPASS_ENC_COMB_CHECK
#include "enccomb_single.h"
#endif

void convert_block(ir_endpoint * endpoints, ir_weight * weights, ir_texel * out,
                   uint8_t bs) {
    for (uint8_t i = 0; i < bs; i++) {
        out[i] = ir_texel_interp(endpoints[weights[i].part], weights[i].weight);
    }
}

ir_fmt texel_mse(ir_texel * orig, ir_texel * comp, uint8_t bs) {
    ir_fmt s = ir_zero;
    for (uint8_t i = 0; i < bs; i++) {
        s += (orig[i].r - comp[i].r) * (orig[i].r - comp[i].r) +
             (orig[i].g - comp[i].g) * (orig[i].g - comp[i].g) +
             (orig[i].b - comp[i].b) * (orig[i].b - comp[i].b);
    }
    return s / (bs * 3);
}

int find_best_encoding_comb(ir_texel * orig, ir_endpoint * eps, ir_weight * wts,
                            uint8_t parts, uint8_t bs, float * err,
                            uint8_t * comb) {
    ir_fmt tmp_err = ir_type_max;
#ifdef BYPASS_ENC_COMB_CHECK
    uint8_t * combs = (uint8_t *)get_enc_combination_single(parts);
#else
    uint8_t * combs = (uint8_t *)get_enc_combinations(parts);
#endif
    if (!combs)
        return -EINVAL;
    assert(bs == 16);
    while (combs[0] != 255) {
        ir_texel quant[bs];
        // dc and dw are the step sizes of the quantized colour and weight data
        // respectively
        ir_fmt dc = ((ir_fmt)255) / (get_quant_level(comb[ENC_COLOR]) - 1);
        ir_fmt dw = ((ir_fmt)1) / (get_quant_level(comb[ENC_WEIGHT]) - 1);
        ir_texel_quantize(eps, wts, bs, parts, dc, dw, quant);
        ir_fmt mse = texel_mse(orig, quant, bs);
        if (part_verbose_log) {
            printf("Encoding %s (dc: %3.2f) + %s (dw: %3.2f):\n",
                   get_quant_level_string(combs[ENC_COLOR]), dc,
                   get_quant_level_string(combs[ENC_WEIGHT]), dw);
            printf("parts > ");
            for (int i = 0; i < bs; i++) {
                printf("%d ", wts[i].part);
            }
            printf("\n");
            for (int i = 0; i < bs; i++) {
                printf("    px    > %d (p%d)\n", i, wts[i].part);
                printf("    data  > %3.2f,%3.2f,%3.2f -> %3.2f,%3.2f,%3.2f\n",
                       orig[i].r, orig[i].g, orig[i].b, quant[i].r, quant[i].g,
                       quant[i].b);
                printf("    ep    > %3.2f,%3.2f,%3.2f -- %3.2f,%3.2f,%3.2f\n",
                       eps[wts[i].part].a.r, eps[wts[i].part].a.g,
                       eps[wts[i].part].a.b, eps[wts[i].part].b.r,
                       eps[wts[i].part].b.g, eps[wts[i].part].b.b);
                printf("    wt    > %1.4f\n\n", wts[i].weight);
            }
        }

        // If the MSE of this encoding is the lowest, save the parameters
        if (mse < tmp_err) {
            tmp_err = mse;
            comb[ENC_COLOR] = combs[ENC_COLOR];
            comb[ENC_WEIGHT] = combs[ENC_WEIGHT];
        }
        combs += 2;
    }
    if (part_verbose_log)
        printf("Best encodings: %s + %s\n",
               get_quant_level_string(comb[ENC_COLOR]),
               get_quant_level_string(comb[ENC_WEIGHT]));
    *err = tmp_err;
    return 0;
}

int find_single_partition(uint8_t bw, uint8_t bh, ir_texel * orig, float * err,
                          ir_endpoint * endpoints, ir_weight * weights,
                          uint8_t * enc_comb) {
    uint8_t bs = bw * bh;

    switch (block_get_ep(orig, NULL, bs, endpoints)) {
    case EP_STATE_SAME_EP:
        return EP_STATE_SAME_EP;
    default:
        break;
    }

    for (int i = 0; i < bs; i++) {
        weights[i].weight = ir_ep_calc_wt(endpoints[0], orig[i]);
        weights[i].part = 0;
        check_valid_weight(&weights[i]);
    }
    if (err && enc_comb) {
        find_best_encoding_comb(orig, endpoints, weights, 1, bs, err, enc_comb);
    }
    return 0;
}

int reorder_blk(int bs, int parts, uint8_t * in, uint8_t * out) {
    uint8_t norm[bs];
    uint8_t map[parts];
    for (int k = 0; k < parts; k++) {
        map[k] = 255;
    }
    int ix = 0;
    for (int k = 0; k < bs; k++) {
        if (map[in[k]] == 255)
            map[in[k]] = ix++;
    }
    if (ix == 1)
        return 1;
    for (int k = 0; k < bs; k++) {
        out[k] = map[in[k]];
        assert(out[k] < 4);
    }
    return 0;
}

int find_closest_partition(uint8_t bw, uint8_t bh, uint8_t parts,
                           uint8_t * ideal) {
    uint8_t * data = (uint8_t *)get_part_data(bw, bh, parts);
    uint16_t ct = get_part_count(bw, bh, parts);
    uint8_t bs = bw * bh;
    if (!data)
        return -EINVAL;

    /* We can reorder endpoints, so normalize the partition order. */
    uint8_t ideal_tmp[bs];
    uint8_t cmp_tmp[bs];
    reorder_blk(bs, parts, ideal, ideal_tmp);

    int best_part = -1;
    int best_similar = 0;
    for (int p = 0; p < ct; p++) {
        int similar = 0;
        reorder_blk(bs, parts, data + p * bs, cmp_tmp);
        for (int i = 0; i < bs; i++) {
            similar += (ideal_tmp[i] == cmp_tmp[i]);
        }
        if (similar > best_similar) {
            best_similar = similar;
            best_part = p;
        }
    }
    return best_part;
}

int find_best_partition(uint8_t bw, uint8_t bh, uint8_t parts, uint16_t from,
                        uint16_t to, ir_texel * orig, float * err,
                        ir_endpoint * endpoints, ir_weight * weights,
                        uint8_t * enc_comb) {
    uint8_t * data = (uint8_t *)get_part_data(bw, bh, parts);
    uint8_t bs = bw * bh;
    if (!data)
        return -EINVAL;

    int best_part = -1;
    uint8_t part_mask[bs];
    ir_weight tmp_wt[bs];
    ir_endpoint tmp_ep[parts];
    ir_texel tmp_out[bs];
    for (int p = from; p <= to; p++) {
        // Current partition assignments
        uint8_t * part = data + p * bs;

        // Calculate endpoint values for each partition
        for (int ep = 0; ep < parts; ep++) {
            // Mask off texels not part of the current partition
            for (int i = 0; i < bs; i++) {
                part_mask[i] = part[i] == ep;
            }
            // Calculate endpoint value
            block_get_ep(orig, part_mask, bs, &tmp_ep[ep]);
        }

        // Calculate weight data
        for (int i = 0; i < bs; i++) {
            tmp_wt[i].weight = ir_ep_calc_wt(tmp_ep[part[i]], orig[i]);
            tmp_wt[i].part = part[i];
            check_valid_weight(&tmp_wt[i]);
        }

        // Calculate the best combination of parameters for this block
        if (err && enc_comb) {
#ifdef BYPASS_ENC_COMB_CHECK
            uint8_t * combs = (uint8_t *)get_enc_combination_single(parts);
#else
            uint8_t * combs = (uint8_t *)get_enc_combinations(parts);
#endif
            while (combs[0] != 255) {
                // dc and dw are the step sizes of the quantized colour and
                // weight data respectively
                ir_fmt dc =
                    ((ir_fmt)255) / (get_quant_level(combs[ENC_COLOR]) - 1);
                ir_fmt dw =
                    ((ir_fmt)1) / (get_quant_level(combs[ENC_WEIGHT]) - 1);
                ir_texel_quantize(tmp_ep, tmp_wt, bs, parts, dc, dw, tmp_out);
                ir_fmt mse = texel_mse(orig, tmp_out, bs);

                // If the MSE of this encoding is the lowest, save the
                // parameters
                if (mse < *err) {
                    *err = mse;
                    best_part = p;
                    for (int i = 0; i < parts; i++) {
                        memcpy(&endpoints[i], &tmp_ep[i], sizeof(ir_endpoint));
                    }
                    for (int i = 0; i < bs; i++) {
                        memcpy(&weights[i], &tmp_wt[i], sizeof(ir_weight));
                    }
                    enc_comb[ENC_COLOR] = combs[ENC_COLOR];
                    enc_comb[ENC_WEIGHT] = combs[ENC_WEIGHT];
                }
                combs += 2;
            }
        }
    }

    return best_part;
}