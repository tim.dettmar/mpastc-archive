#include "pca.h"

#define DISABLE_PCA

/*  Covariance of texel data colour channels

    texels: Array of texels containing n elements
    mask:   Element mask
    n:      Number of elements, including masked elements
    out:    Colour channel covariance matrix
            (3x3 matrix with rr rg rb / br bg bb / gr gg gb format)
*/
void cov(ir_texel * texels, uint8_t * mask, uint8_t n, ir_texel * out) {
    uint8_t nn = 0;
    ir_texel means = {0, 0, 0};
    memset(out, 0, sizeof(*out) * 3);
    for (uint8_t k = 0; k < n; k++) {
        if (!mask || mask[k]) {
            means = ir_texel_addv(means, texels[k]);
            nn++;
        }
    }
    means = ir_texel_div(means, (ir_fmt)nn);
    ir_texel vals[n];
    for (uint8_t k = 0; k < n; k++) {
        vals[k] = ir_texel_subv(texels[k], means);
    }

    enum { r, g, b };

    for (uint8_t k = 0; k < n; k++) {
        if (!mask || mask[k]) {
            out[r].r += vals[k].r * vals[k].r;
            out[r].g += vals[k].r * vals[k].g;
            out[r].b += vals[k].r * vals[k].b;

            out[g].r += vals[k].g * vals[k].r;
            out[g].g += vals[k].g * vals[k].g;
            out[g].b += vals[k].g * vals[k].b;

            out[b].r += vals[k].b * vals[k].r;
            out[b].g += vals[k].b * vals[k].g;
            out[b].b += vals[k].b * vals[k].b;
        }
    }

    for (uint8_t k = 0; k < 3; k++) {
        out[k] = ir_texel_div(out[k], nn);
        check_valid_texel(&out[k]);
    }
}

/*  Matrix vector multiplication

    a: Covariance matrix
    x: Vector with rgb layout (1x ir_texel)

    Returns the multiplication result.
*/
ir_texel mvmul(ir_texel * a, ir_texel x) {
    ir_texel ret;
    ret.r = ir_texel_dot(a[0], x);
    ret.g = ir_texel_dot(a[1], x);
    ret.b = ir_texel_dot(a[2], x);
    return ret;
}

ir_fmt ir_abs(ir_fmt a) {
#if ir_fmt == float || ir_fmt == double
    return signbit(a) ? -a : a;
#else
    return a >= 0 ? a : -a;
#endif
}

/*  Calculate eigenvector

    m:          Covariance matrix
    max_iter:   Maximum number of iterations
    eps:        Minimum change (stopping criterion)

    Returns:    Eigenvector (normalized)
                If no eigenvector is findable (e.g. orthogonal texel data) then
                a vector with nan values are returned)
*/
ir_texel ev(ir_texel * m, int max_iter, ir_fmt eps) {
    ir_texel iv = {1.0f, 2.0f, 3.0f}; // arbitrary init vector
    while (max_iter) {
        iv = mvmul(m, iv);
        ir_fmt mag = ir_texel_magnitude(iv);
        if (mag < 0.01f) {
            ir_texel ret = {NAN, NAN, NAN};
            return ret;
        }
        assert(isfinite(mag) && mag != 0);
        iv = ir_texel_div(iv, mag);
        max_iter--;
    }
    return ir_texel_normalize(iv);
}

int block_get_ep(ir_texel * texels, uint8_t * mask, uint8_t n,
                 ir_endpoint * out) {

    uint8_t nn = 0; // actual used value count
    uint8_t use_min_max = 0;
    uint8_t idx = 0;
    ir_texel mv = {ir_zero, ir_zero, ir_zero}; // channel mean values

#ifndef DISABLE_PCA
    /* Check if the values are identical - this would result in an invalid
     * covariance matrix and break the calculations, so we take the fast path */
    ir_texel prev = {-1.0f, -1.0f, -1.0f};
    for (uint8_t i = 0; i < n; i++) {
        if (!mask || mask[i]) {
            if (prev.r < 0.0f) {
                prev = texels[i];
                idx = i;
            } else {
                nn += !ir_texel_approx_equal(texels[i], prev, 1.0f);
            }
        }
    }

    if (!nn) {
        out->a = texels[idx];
        out->b = texels[idx];
        return EP_STATE_SAME_EP;
    }

    check_valid_texels(texels, n);
    if (pca_verbose_log)
        printf("Texels: ");
    for (uint8_t i = 0; i < n; i++) {
        if (!mask || mask[i]) {
            if (pca_verbose_log)
                printf("%3.2f,%3.2f,%3.2f ", texels[i].r, texels[i].g,
                       texels[i].b);
            mv.r += texels[i].r;
            mv.g += texels[i].g;
            mv.b += texels[i].b;
            nn++;
        }
    }
    if (pca_verbose_log)
        printf("\n");

    assert(nn);

    // Calculate eigenvectors
    ir_texel eig;
    ir_texel cov_out[3];
    {
        cov(texels, mask, n, cov_out);
        eig = ev(cov_out, 10, 0.0001f);
    }
    if (pca_verbose_log)
        printf("Eigenvector: %3.2f,%3.2f,%3.2f\n", eig.r, eig.g, eig.b);

    /* Eigenvector nonexistent, use min-max instead of vector projection for
     * endpoint calculation */
    if (!isfinite(eig.r) || !isfinite(eig.g) || !isfinite(eig.b)) {
        use_min_max = 1;
    } else {
        check_valid_texel(&eig);
    }
#else
    use_min_max = 1;
#endif

    // Calculate the endpoint values by projecting the centered texels onto the
    // eigenvector
    uint8_t smin_idx = 254;
    uint8_t smax_idx = 255;
    ir_fmt smin = INFINITY;
    ir_fmt smax = -INFINITY;
    if (use_min_max) {
        for (int i = 0; i < n; i++) {
            if (!mask || mask[i]) {
                ir_fmt s = ir_texel_magnitude(texels[i]);
                if (s < smin) {
                    out->a = texels[i];
                    smin = s;
                    smin_idx = i;
                }
                if (s > smax) {
                    out->b = texels[i];
                    smax = s;
                    smax_idx = i;
                }
            }
        }
        if (smin_idx == smax_idx) {
            out->a = texels[smin_idx];
            out->b = texels[smin_idx];
            return EP_STATE_SAME_EP;
        }
    }
#ifndef DISABLE_PCA
    else {
        for (int i = 0; i < n; i++) {
            if (!mask || mask[i]) {
                ir_fmt s = ir_texel_dot(texels[i], eig);
                if (s < smin) {
                    smin = s;
                    out->a = ir_texel_proj(texels[i], eig);
                    // out->a = texels[i];
                    check_valid_texel(&out->a);
                }
                if (s > smax) {
                    smax = s;
                    out->b = ir_texel_proj(texels[i], eig);
                    // out->a = texels[i];
                    check_valid_texel(&out->b);
                }
            }
        }
        if (pca_verbose_log)
            printf("\nsmin: %f, max: %f\n", smin, smax);
    }
#endif

    if (pca_verbose_log)
        printf("Endpoints: %3.2f,%3.2f,%3.2f / %3.2f,%3.2f,%3.2f\n", out->a.r,
               out->a.g, out->a.b, out->b.r, out->b.g, out->b.b);
    check_valid_ep(out);

    // Avoid ASTC blue contraction encoding
    if (ir_texel_sum(out->a) > ir_texel_sum(out->b)) {
        ir_texel tmp = out->a;
        out->a = out->b;
        out->b = tmp;
        return EP_STATE_FLIPPED;
    }

    return EP_STATE_OK;
}