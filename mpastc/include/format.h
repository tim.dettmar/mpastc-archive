#ifndef MPASTC_FORMAT_H_
#define MPASTC_FORMAT_H_

typedef enum {
    TEX_FMT_NONE,
    TEX_FMT_RGB_888, // 24-bit RGB
    TEX_FMT_RGB_IR,  // 32bpc RGB intermediate float
    TEX_FMT_MAX
} tex_fmt;

#endif