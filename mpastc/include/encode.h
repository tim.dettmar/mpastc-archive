#ifndef MPASTC_ENCODE_H_
#define MPASTC_ENCODE_H_

#include <assert.h>
#include <errno.h>
#include <math.h>
#include <stdint.h>

#include "astc.h"
#include "texel.h"

#include "astcenc_interop.h"

#ifdef __cplusplus
extern "C" {
#endif

void encode_astc_block(ir_endpoint * eps, ir_weight * wts, uint8_t parts,
                       uint16_t part_index, quant_method weight_range,
                       quant_method colour_range, uint8_t * out);

/* Encode a void-extent block consisting of a single colour. This function
 * always writes exactly 128 bits to the output buffer. */
void encode_void_extent(ir_texel colour, void * out);

uint64_t encode_rgb_endpoints(ir_endpoint * ep, uint8_t parts, quant_method r,
                              uint64_t offset, uint8_t * out);

uint64_t encode_weights(ir_weight * wts, ir_endpoint * eps, quant_method r,
                        uint8_t * block_base);

uint64_t get_bitcount(uint8_t parts, uint8_t n_wts, quant_method wt_range,
                      quant_method col_range, uint8_t * hdr, uint8_t * wt,
                      uint8_t * col);

/*  Encode a group of values using integer sequence encoding.

    count:  Number of values to encode
    offset: Offset in bits to start writing to the output
    range:  Range to use for encoding
    vals:   Input values
    out:    Output buffer

    Returns the number of actual bits written to the output buffer (not
    including the offset).
*/
uint64_t encode_bise(uint64_t count, uint64_t offset, quant_method range,
                     uint8_t * vals, uint8_t * out);

int set_block_header(uint8_t w, uint8_t h, uint8_t dp, uint8_t parts,
                     uint16_t part_index, quant_method wt_range,
                     uint64_t offset, uint8_t * out);

int read_block_mode(uint16_t mode, uint8_t * width, uint8_t * height,
                    uint8_t * dp, uint8_t * quant_method);

/* Available bits for encoding colour and texel data */
static inline uint8_t get_avail_enc_bits(uint8_t partitions, uint8_t cem,
                                         uint8_t dp) {
    switch (partitions) {
    case 1:
        // 128 - 11 (block mode) - 2 (part) - 4 (CEM)
        return 111;
    case 2:
    case 3:
    case 4:
        // 128 - 11 (block mode) - 2 (part) - 10 (part#) - 6 (CEM)
        return 99;
    }
}

uint64_t calc_bitcount(quant_method r, uint32_t n_values);

#ifdef __cplusplus
}
#endif

#endif