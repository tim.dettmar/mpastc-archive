#ifndef MPASTC_PCA_H_
#define MPASTC_PCA_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <math.h>
#include <assert.h>
#include <string.h>

#include "astc.h"
#include "texel.h"

enum ep_state {
    EP_STATE_OK = 0,
    EP_STATE_SAME_EP = 1,
    EP_STATE_FLIPPED = 2
};

static int pca_verbose_log = 0;

/*  Calculate the eigenvector for a texel covariance matrix using power
    iteration. The function iterates until either the iteration changed the
    vector sum < eps, or max_iter iterations have been reached.

    m:          3x3 matrix with rgb/rgb/rgb layout
    max_iter:   Maximum iterations
    eps:        Convergence criteria
*/
ir_texel ev(ir_texel * m, int max_iter, ir_fmt eps);

/*  Covariance of texel data colour channels

    texels: Array of texels containing n elements
    mask:   Blocks to include/exclude from calculation (optional)
    n:      Number of elements
    out:    Colour channel covariance matrix
            (3x3 matrix with rr rg rb / br bb bg / gr gb gg format)
*/
void cov(ir_texel * texels, uint8_t * mask, uint8_t n, ir_texel * out);

/*  Get the endpoint colours for a block.
    texels: Input texel data (processed)
    mask:   Blocks to include/exclude from calculation (optional)
    n:      Number of texels (block width x height)
    out:    Output endpoints

    Note:   The endpoints A and B are ordered such that blue contraction
            encoding is never used. Thus, sum(ep_a) < sum(ep_b).

    Returns: 0 on success, 1 if the endpoints are the same, 2 if the endpoints
             were flipped.
*/
int block_get_ep(ir_texel * texels, uint8_t * mask, uint8_t n,
                 ir_endpoint * out);

#ifdef __cplusplus
}
#endif

#endif