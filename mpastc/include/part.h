#ifndef MPASTC_PART_H_
#define MPASTC_PART_H_

#ifdef __cplusplus
extern "C" {
#endif

static int part_verbose_log = 0;

#include "config.h"
#include "enccomb.h"
#include "pca.h"
#include "texel.h"

#include <errno.h>
#include <math.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

/*  Convert a block from endpoint + weight representation to raw data.

    endpoints: Endpoint colours
    weights:   Texel weights and partitions
    out:       Raw texel data
    bs:        Block size

    Note: The number of partitions is implicitly determined from the input data.
    The length of the endpoints array must be >= the number of partitions in the
    weight grid.

    Warning: This function does not support interpolation of the weight grid!
    The weight grid width/height must equal the block width/height!
*/
void convert_block(ir_endpoint * endpoints, ir_weight * weights, ir_texel * out,
                   uint8_t bs);

/*  Calculate the MSE between two blocks
    orig:    Original texel data
    comp:    Decoded compressed texel data
    bs:      Block size (width x height)

    Returns: Mean squared error
*/
ir_fmt texel_mse(ir_texel * orig, ir_texel * comp, uint8_t bs);

/*  Find the best combination of weight/colour bit allocations for the block.
    orig:   Original texel data
    eps:    Colour endpoint data
    wts:    Weight data
    parts:  Number of partitions
    bs:     Block size
    err:    MSE of the best encoding
    comb:   Output pair of the best combination ranges
*/
int find_best_encoding_comb(ir_texel * orig, ir_endpoint * eps, ir_weight * wts,
                            uint8_t parts, uint8_t bs, float * err,
                            uint8_t * comb);

/*  Find the best endpoints for a single-partition block configuration.
    bw:         Block width
    bh:         Block height
    orig:       Texel data
    err:        Output MSE
    endpoints:  Output endpoints (length 1)
    weights:    Output weights (length bw * bh)
    enc_comb:   Index of the best encoding parameter combination (cf.
                enc_table_* in enccomb.h). If this is NULL, encoding parameter
                checks will not be performed, and only the endpoints/weights are
                returned.

    Returns:    0 or -EINVAL with invalid parameters
*/
int find_single_partition(uint8_t bw, uint8_t bh, ir_texel * orig, float * err,
                          ir_endpoint * endpoints, ir_weight * weights,
                          uint8_t * enc_comb);

/*  Find the partition that most closely matches the ideal grouping of texels,
 *  determined through k-means or a similar method.
    bw:    [in] Block width
    bh:    [in] Block height
    parts: [in] Partition count
    ideal: [in] Ideal partitioning
    Returns: closest partition ID (unmapped to ASTC order!)
 */
int find_closest_partition(uint8_t bw, uint8_t bh, uint8_t parts,
                           uint8_t * ideal);

/*  Find the best partitioning for a group of texels.
    bw:         [in]  Block width
    bh:         [in]  Block height
    parts:      [in]  Partition count
    from:       [in]  Starting partition index (inclusive)
    to:         [in]  End partition index (inclusive)
    orig:       [in]  Original texel data of length bw * bh
    err:        [out] Output MSE
    endpoints:  [out] Best endpoints found within given partition range
    weights:    [out] Best weights found for best partition
    enc_comb:   [out] Index of the best encoding parameter combination (cf.
                      enc_table_* in enccomb.h) If this is NULL, encoding
                      parameter checks will not be performed, and only the
                      endpoints/weights are returned.

    Returns:    best partition ID within range [from, to]
                If all partitions within the range [from, to] are masked out, -1
                is returned.
*/
int find_best_partition(uint8_t bw, uint8_t bh, uint8_t parts, uint16_t from,
                        uint16_t to, ir_texel * orig, float * err,
                        ir_endpoint * endpoints, ir_weight * weights,
                        uint8_t * enc_comb);

#ifdef __cplusplus
}
#endif

#endif