#ifndef MPASTC_INTERFACE_H_
#define MPASTC_INTERFACE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include <stdint.h>

#include "format.h"
#include "encode.h"
#include "pca.h"

int astc_compress_block(void * in, void * out, tex_fmt in_fmt,
                        uint8_t bw, uint8_t bh);


#ifdef __cplusplus
}
#endif

#endif