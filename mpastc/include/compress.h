#include <stdint.h>

enum buffer_format { FMT_INVALID, FMT_RGBA = 1, FMT_ASTC = 2, FMT_MAX };

struct buffer {
    void * buf;
    enum buffer_format fmt;
    uint32_t width;
    uint32_t height;
    union {
        uint32_t pixel_pitch;
        uint32_t block_width;
    };
    union {
        uint32_t row_stride;
        uint32_t block_height;
    };
};