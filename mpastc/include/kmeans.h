#ifndef MPASTC_KMEANS_H_
#define MPASTC_KMEANS_H_

#include "astc.h"
#include "texel.h"

#include <assert.h>
#include <errno.h>
#include <random>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/*  Perform k-means clustering on texel data with k-means++ initial centroid
    selection.

    texels: Texel data of length bs
    k: Number of clusters
    bs: Block size (width x height)
    max_iter: Maximum iterations
    clusters: Output cluster assignments

    Returns 0 on convergence, 1 if max_iter reached, -EINVAL with invalid
    arguments.
*/
int kmeans_pp(ir_texel * texels, int8_t k, uint8_t bs, uint32_t max_iter,
              uint8_t * clusters);

/*  Perform k-means clustering on texel data

    texels: Texel data of length bs
    init: Initial means of length k. If null, they will be randomly generated.
    k: Number of clusters
    bs: Block size (width x height)
    max_iter: Maximum iterations
    clusters: Output cluster assignments

    The algorithm stops if either convergence or max_iter iterations have been
    reached.

    This function is not designed for k > 4.

    Returns 0 on convergence, 1 if max_iter reached, -EINVAL with invalid
    arguments.
*/
int kmeans(ir_texel * texels, ir_texel * init, int8_t k, uint8_t bs,
           uint32_t max_iter, uint8_t * clusters);

#endif