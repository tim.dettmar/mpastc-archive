#ifndef MPASTC_BITWRITER_H_
#define MPASTC_BITWRITER_H_

#include <stdint.h>
#include <string.h>

/*  Write an arbitrary number of bits.
    src:    Source pointer
    dst:    Destination pointer
    offset: Offset in bits to start writing to the destination pointer
    n:      Number of bits to write to the destination
*/
static void writebits(uint8_t * src, uint8_t * dst, uint64_t offset,
                      uint64_t n) {
    uint8_t * dptr = dst + offset / 8;
    const uint8_t lb = offset % 8;
    const uint8_t orig_lo = (1 << lb) - 1; // ~orig_lo = orig_hi
    int i = 0;
    while (n > 0) {
        uint16_t to_write = src[i] << lb;
        dptr[i] = (dptr[i] & orig_lo) | (uint8_t)(to_write & 0xFF);
        if (n > (uint64_t)(8 - lb)) {
            dptr[i + 1] =
                (dptr[i + 1] & (~orig_lo)) | (uint8_t)((to_write >> 8) & 0xFF);
        }
        i++;
        if (n < 8)
            n = 0;
        else
            n -= 8;
    }
}

#endif