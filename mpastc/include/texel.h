#ifndef MPASTC_TEXEL_H_
#define MPASTC_TEXEL_H_

#include <assert.h>
#include <math.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "astcenc_interop.h"

static int texel_verbose_log = 0;

/*  Intermediate texel representation. Using uint8_t for intermediate
    calculations leads to a significant loss of accuracy. The texels are thus
    cast into a higher-precision representation and downsampled before storage.
*/

#define ir_fmt float          // Intermediate texel datatype
#define ir_zero 0.0f          // Minimum luminance value
#define ir_texel_min 0.0f     // Minimum luminance value
#define ir_texel_max 255.0f   // Maximum luminance value
#define ir_weight_min 0.0f    // Minimum weight value
#define ir_weight_max 1.0f    // Maximum weight value
#define ir_type_max INFINITY  // Maximum value of the datatype
#define ir_type_min -INFINITY // Minimum value of the datatype

#define ir_black                                                               \
    { 0.0f, 0.0f, 0.0f }
#define ir_white                                                               \
    { 255.0f, 255.0f, 255.0f }

/* Intermediate texel representation */
typedef struct {
    ir_fmt r;
    ir_fmt g;
    ir_fmt b;
} ir_texel;

/* Intermediate weight representation */
typedef struct {
    ir_fmt weight; // Weight value from 0.0f - 1.0f
    uint8_t part;  // Partition number from 0 - 3
} ir_weight;

/* Intermediate endpoint representation */
typedef struct {
    ir_texel a;
    ir_texel b;
} ir_endpoint;

/* 24bpt, 8bpc texel representation */
#pragma pack(push, 1)
typedef struct {
    union {
        uint8_t r;
        uint8_t x;
    };
    union {
        uint8_t g;
        uint8_t y;
    };
    union {
        uint8_t b;
        uint8_t z;
    };
} texel;
#pragma pack(pop)

static void check_valid_ep(ir_endpoint * ep) {
#ifndef NDEBUG
    assert(isfinite(ep->a.r));
    assert(isfinite(ep->a.g));
    assert(isfinite(ep->a.b));
    assert(isfinite(ep->b.r));
    assert(isfinite(ep->b.g));
    assert(isfinite(ep->b.b));
#endif
}

static void check_valid_texel(ir_texel * tex) {
#ifndef NDEBUG
    assert(isfinite(tex->r));
    assert(isfinite(tex->g));
    assert(isfinite(tex->b));
#endif
}

static void check_valid_texels(ir_texel * tex, uint8_t n) {
#ifndef NDEBUG
    for (uint8_t i = 0; i < n; i++) {
        check_valid_texel(&tex[i]);
    }
#endif
}

static void check_valid_weight(ir_weight * wt) {
#ifndef NDEBUG
    assert(isfinite(wt->weight));
    assert(wt->part >= 0 && wt->part < 4);
#endif
}

static void check_valid_weights(ir_weight * wt, uint8_t n) {
#ifndef NDEBUG
    for (uint8_t i = 0; i < n; i++) {
        check_valid_weight(&wt[i]);
    }
#endif
}

static inline ir_texel ir_texel_create(ir_fmt r, ir_fmt g, ir_fmt b) {
    ir_texel ret;
    ret.r = r;
    ret.g = g;
    ret.b = b;
    return ret;
}

static inline ir_fmt ir_clamp(ir_fmt val, ir_fmt minv, ir_fmt maxv) {
    if (val < minv)
        return minv;
    if (val > maxv)
        return maxv;
    return val;
}

static inline ir_texel ir_texel_clamp(ir_texel tex) {
    ir_texel ret;
    ret.r = ir_clamp(tex.r, ir_texel_min, ir_texel_max);
    ret.g = ir_clamp(tex.g, ir_texel_min, ir_texel_max);
    ret.b = ir_clamp(tex.b, ir_texel_min, ir_texel_max);
    return ret;
}

static inline ir_weight ir_weight_clamp(ir_weight wt) {
    ir_weight ret;
    ret.weight = ir_clamp(wt.weight, ir_weight_min, ir_weight_max);
    return ret;
}

static inline ir_fmt ir_texel_euc_dist(ir_texel a, ir_texel b) {
    return sqrt((b.r - a.r) * (b.r - a.r) + (b.g - a.g) * (b.g - a.g) +
                (b.b - a.b) * (b.b - a.b));
}

static inline ir_texel ir_texel_interp(ir_endpoint ep, ir_fmt weight) {
    ir_texel ret;
    ret.r = ep.a.r + ((ep.b.r - ep.a.r) * weight);
    ret.g = ep.a.g + ((ep.b.g - ep.a.g) * weight);
    ret.b = ep.a.b + ((ep.b.b - ep.a.b) * weight);
    return ret;
}

static inline uint8_t clamp_range(uint8_t val, quant_method r) {
    if (val > get_quant_level(r) - 1) {
        return get_quant_level(r) - 1;
    }
    return val;
}

static inline ir_texel ir_texel_mul(ir_texel a, ir_fmt x) {
    ir_texel ret;
    ret.r = a.r * x;
    ret.g = a.g * x;
    ret.b = a.b * x;
    return ret;
}

static inline ir_texel ir_texel_div(ir_texel a, ir_fmt x) {
    ir_texel ret;
    ret.r = a.r / x;
    ret.g = a.g / x;
    ret.b = a.b / x;
    return ret;
}

static inline ir_texel ir_texel_add(ir_texel a, ir_fmt x) {
    ir_texel ret;
    ret.r = a.r + x;
    ret.g = a.g + x;
    ret.b = a.b + x;
    return ret;
}

static inline ir_texel ir_texel_sub(ir_texel a, ir_fmt x) {
    ir_texel ret;
    ret.r = a.r - x;
    ret.g = a.g - x;
    ret.b = a.b - x;
    return ret;
}

static inline ir_texel ir_texel_abs(ir_texel a) {
    ir_texel ret;
    ret.r = a.r < 0 ? -a.r : a.r;
    ret.g = a.g < 0 ? -a.g : a.g;
    ret.b = a.b < 0 ? -a.b : a.b;
    return ret;
}

static inline ir_fmt ir_mod(ir_fmt a, ir_fmt b) { return fmodf(a, b); }

/* Return the dot product of two texels. */
static inline ir_fmt ir_texel_dot(ir_texel a, ir_texel b) {
    return a.r * b.r + a.g * b.g + a.b * b.b;
}

/* Element-wise sum of texel channels */
static inline ir_fmt ir_texel_sum(ir_texel tex) {
    return tex.r + tex.g + tex.b;
}

/* Calculate the magnitude of the texel (vector) a */
static inline ir_fmt ir_texel_magnitude(ir_texel a) {
    return (ir_fmt)sqrt(ir_texel_dot(a, a));
}

/* Return the unit vector for texel a. */
static inline ir_texel ir_texel_normalize(ir_texel a) {
    return ir_texel_div(a, ir_texel_magnitude(a));
}

static inline int ir_texel_equal(ir_texel a, ir_texel b) {
    return ((a.r == b.r) && (a.g == b.g) && (a.b == b.b));
}

static inline int ir_texel_approx_equal(ir_texel a, ir_texel b, ir_fmt eps) {
    return ir_texel_euc_dist(a, b) < eps;
}

/* Subtract a from b element-wise */
static inline ir_texel ir_texel_subv(ir_texel a, ir_texel b) {
    ir_texel ret;
    ret.r = a.r - b.r;
    ret.g = a.g - b.g;
    ret.b = a.b - b.b;
    return ret;
}

static inline ir_texel ir_texel_copy(ir_texel a) { return a; }

static inline texel texel_copy(texel a) { return a; }

static inline ir_texel ir_texel_delta(ir_texel a, ir_texel b) {
    return ir_texel_abs(ir_texel_subv(a, b));
}

/* Add from b element-wise */
static inline ir_texel ir_texel_addv(ir_texel a, ir_texel b) {
    ir_texel ret;
    ret.r = a.r + b.r;
    ret.g = a.g + b.g;
    ret.b = a.b + b.b;
    return ret;
}

static inline int ir_ep_equal(ir_endpoint x, float eps) {
    ir_texel d = ir_texel_delta(x.a, x.b);
    return ir_texel_sum(d) > eps;
}

/* Convert the endpoint line segment into a vector */
static inline ir_texel ir_ep_vectorize(ir_endpoint ep) {
    return ir_texel_subv(ep.b, ep.a);
}

/*  Perform a scalar projection of the texel data over the endpoint ep. */
static inline ir_fmt ir_ep_sproj(ir_endpoint ep, ir_texel tex) {
    ir_texel uv = ir_ep_vectorize(ep);
    ir_texel ct = ir_texel_subv(tex, ep.a);
    return ir_texel_dot(ct, ir_texel_div(uv, ir_texel_magnitude(uv)));
}

static inline ir_texel ir_texel_proj(ir_texel a, ir_texel b) {
    return ir_texel_mul(b, ir_texel_dot(a, b) / ir_texel_dot(b, b));
}

/* Calculate the weight for texel tex, given endpoint ep. Automatically clamps
 * the range within [0.0f, 1.0f]. */
static inline ir_fmt ir_ep_calc_wt(ir_endpoint ep, ir_texel tex) {
    ir_texel uv = ir_ep_vectorize(ep);
    if (ir_texel_magnitude(uv) < 0.1f)
        return 0.0f;
    ir_texel ct = ir_texel_subv(tex, ep.a);
    ir_fmt ratio = ir_texel_dot(ct, ir_texel_div(uv, ir_texel_magnitude(uv))) /
                   ir_texel_magnitude(uv);
    if (ratio < 0.0f) {
        ratio = -ratio;
    }
    return ir_clamp(ratio, (ir_fmt)0, (ir_fmt)1);
}

/* Access texels by index (r=0, g=1, b=2) */
static inline ir_fmt ir_texel_idx(ir_texel tex, uint8_t index) {
    switch (index) {
    case 0:
        return tex.r;
    case 1:
        return tex.g;
    case 2:
        return tex.b;
    default:
        return -1;
    }
};

/* Set texel values by numeric index (r=0, g=1, b=2) */
static inline void ir_texel_set(ir_texel * tex, uint8_t index, ir_fmt val) {
    switch (index) {
    case 0:
        tex->r = val;
        return;
    case 1:
        tex->g = val;
        return;
    case 2:
        tex->b = val;
        return;
    default:
        return;
    }
}

static inline ir_fmt ir_quantize_value(ir_fmt val, ir_fmt delta) {
    ir_fmt r = delta / 2;
    ir_fmt m = ir_mod(val, delta);
    if (texel_verbose_log)
        printf("v: %.3f, d: %.3f, r = %.3f, m = %.3f, up = %.3f, down = %.3f, ",
               val, delta, r, m, val + (delta - m), val - m);
    if (m > r) {
        if (texel_verbose_log)
            printf("-> up\n");
        return val + (delta - m);
    } else {
        if (texel_verbose_log)
            printf("-> down\n");
        return val - m;
    }
}

/*  Convert the RGB value from storage to intermediate representation. */
static inline void ir_upsample_rgb(texel * storage, ir_texel * ir,
                                   uint64_t texels) {
    for (uint_fast8_t i = 0; i < texels; i++) {
        ir[i].r = (ir_fmt)storage[i].r;
        ir[i].g = (ir_fmt)storage[i].g;
        ir[i].b = (ir_fmt)storage[i].b;
    }
}

/*  Convert the RGB value from intermediate to storage representation. */
static inline texel ir_downsample_rgb(ir_texel a, quant_method r) {
    texel ret;
    ir_fmt mul = (ir_fmt)(get_quant_level(r) - 1) / 255.0f;
    ret.r = (uint8_t)clamp_range(a.r * mul, r);
    ret.g = (uint8_t)clamp_range(a.g * mul, r);
    ret.b = (uint8_t)clamp_range(a.b * mul, r);
    return ret;
}

/* Perform a decode of intermediate texel values into raw data. This is
 * primarily for debugging purposes. The number of partitions is implicitly
 * determined from the input data. */
void ir_texel_decode(ir_endpoint * eps, ir_weight * weights, uint8_t bs,
                     texel * out);

/* Convert the raw RGB value into a representation of the quantized value */
void ir_texel_quantize(ir_endpoint * eps, ir_weight * weights, uint8_t bs,
                       uint8_t parts, ir_fmt c_delta, ir_fmt w_delta,
                       ir_texel * quantized);

#endif