#ifndef MPASTC_ASTC_H_
#define MPASTC_ASTC_H_

#ifdef __cplusplus
extern "C" {
#endif

/* The maximum block size is 12 x 12 */
#define MAX_BLOCK_TEXELS 144

/* mpastc only supports the RGB encoding mode */
#define CEM_LDR_RGB_DIRECT 8

#ifdef __cplusplus
}
#endif

#endif