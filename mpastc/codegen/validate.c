#include "partition.h"
#include <stdio.h>
#include <assert.h>

int main(int argc, char ** argv) {
    uint8_t bad = 0;
    for (int p = 2; p < 5; p++) {
        uint8_t * pd = get_part_data(4, 4, p);
        uint16_t pc = get_part_count(4, 4, p);
        uint16_t * pm = get_part_mapping(4, 4, p);
        assert(pd);
        assert(pm);
        assert(pc);

        for (int i = 0; i < pc; i++) {
            uint8_t seen[4] = {0, 0, 0, 0};
            uint8_t * data = pd + i * 16;
            for (int j = 0; j < 16; j++) {
                if (data[j] > 3) {
                    printf("Error: invalid partition value %d at %d-partition "
                           "%d (%d) texel %d\n",
                           data[j], p, i, pm[i], j);
                    bad = 1;
                }
                seen[data[j]] = 1;
            }

            uint8_t ttl = 0;
            for (int k = 0; k < 4; k++) {
                ttl += seen[k];
            }

            if (ttl != p) {
                printf("Error: invalid partition count %d at %d-partition %d (%d)\n",
                       ttl, p, i, pm[i]);
                bad = 1;
            }
        }
    }
    return bad;
}