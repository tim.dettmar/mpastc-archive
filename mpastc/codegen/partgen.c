#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

// const int block_sizes[][2] = {{4, 4},   {5, 4},   {5, 5},   {6, 5}, {6, 6},
//                               {8, 5},   {10, 5},  {10, 6},  {8, 8}, {10, 8},
//                               {10, 10}, {12, 10}, {12, 12}, {0, 0}};

int enable_print;

const int block_sizes[][2] = {{4, 4}, {0, 0}};
const int parts[] = {2, 3, 4, 0};

const int n_sizes = (sizeof(block_sizes) / sizeof(int) / 2) - 1;
const int n_parts = (sizeof(parts) / sizeof(int)) - 1;

/*  hash52 and select_partition are from the Khronos Data Format Specification
    23.21
    https://registry.khronos.org/DataFormat/specs/1.3/dataformat.1.3.pdf
*/

uint32_t hash52(uint32_t p) {
    p ^= p >> 15;
    p -= p << 17;
    p += p << 7;
    p += p << 4;
    p ^= p >> 5;
    p += p << 16;
    p ^= p >> 7;
    p ^= p >> 3;
    p ^= p << 6;
    p ^= p >> 17;
    return p;
}

uint8_t select_partition(int seed, int x, int y, int z, int parts, int ntex) {
    if (ntex < 31) {
        x <<= 1;
        y <<= 1;
        z <<= 1;
    }

    seed += (parts - 1) * 1024;
    uint32_t rnum = hash52(seed);

    uint8_t seed1 = rnum & 0xF;
    uint8_t seed2 = (rnum >> 4) & 0xF;
    uint8_t seed3 = (rnum >> 8) & 0xF;
    uint8_t seed4 = (rnum >> 12) & 0xF;
    uint8_t seed5 = (rnum >> 16) & 0xF;
    uint8_t seed6 = (rnum >> 20) & 0xF;
    uint8_t seed7 = (rnum >> 24) & 0xF;
    uint8_t seed8 = (rnum >> 28) & 0xF;
    uint8_t seed9 = (rnum >> 18) & 0xF;
    uint8_t seed10 = (rnum >> 22) & 0xF;
    uint8_t seed11 = (rnum >> 26) & 0xF;
    uint8_t seed12 = ((rnum >> 30) | (rnum << 2)) & 0xF;

    seed1 *= seed1;
    seed2 *= seed2;
    seed3 *= seed3;
    seed4 *= seed4;
    seed5 *= seed5;
    seed6 *= seed6;
    seed7 *= seed7;
    seed8 *= seed8;
    seed9 *= seed9;
    seed10 *= seed10;
    seed11 *= seed11;
    seed12 *= seed12;
    int sh1, sh2, sh3;
    if (seed & 1) {
        sh1 = (seed & 2 ? 4 : 5);
        sh2 = (parts == 3 ? 6 : 5);
    } else {
        sh1 = (parts == 3 ? 6 : 5);
        sh2 = (seed & 2 ? 4 : 5);
    }
    sh3 = (seed & 0x10) ? sh1 : sh2;

    seed1 >>= sh1;
    seed2 >>= sh2;
    seed3 >>= sh1;
    seed4 >>= sh2;
    seed5 >>= sh1;
    seed6 >>= sh2;
    seed7 >>= sh1;
    seed8 >>= sh2;
    seed9 >>= sh3;
    seed10 >>= sh3;
    seed11 >>= sh3;
    seed12 >>= sh3;
    int a = seed1 * x + seed2 * y + seed11 * z + (rnum >> 14);
    int b = seed3 * x + seed4 * y + seed12 * z + (rnum >> 10);
    int c = seed5 * x + seed6 * y + seed9 * z + (rnum >> 6);
    int d = seed7 * x + seed8 * y + seed10 * z + (rnum >> 2);
    a &= 0x3F;
    b &= 0x3F;
    c &= 0x3F;
    d &= 0x3F;
    if (parts < 4)
        d = 0;
    if (parts < 3)
        c = 0;
    if (a >= b && a >= c && a >= d)
        return 0;
    else if (b >= c && b >= d)
        return 1;
    else if (c >= d)
        return 2;
    else
        return 3;
}

void gen_part(int bw, int bh, int parts, uint8_t * out) {
    int i = 0;
    for (int index = 0; index < 1024; index++) {
        for (int y = 0; y < bh; y++) {
            for (int x = 0; x < bw; x++) {
                out[i++] = select_partition(index, x, y, 0, parts, bw * bh);
            }
        }
    }
}

int reorder_blk(int bs, int parts, uint8_t * in, uint8_t * out) {
    /* Change the order by mapping partition ids based on order seen to
        cover cases like:

        0 0 0 1   1 1 1 0                   0 0 0 1
        0 0 0 1   1 1 1 0   both remapped   0 0 0 1
        0 0 1 1   1 1 0 0   to              0 0 1 1
        0 1 1 1   1 0 0 0                   0 1 1 1

    */
    uint8_t map[parts];
    for (int k = 0; k < parts; k++) {
        map[k] = 255;
    }
    int ix = 0;
    for (int k = 0; k < bs; k++) {
        assert(in[k] < parts);
        if (map[in[k]] == 255)
            map[in[k]] = ix++;
    }

    if (ix == 1)
        return 1;
    if (ix != parts)
        return 2;
    
    for (int k = 0; k < bs; k++) {
        out[k] = map[in[k]];
        assert(out[k] < 4);
    }
    return 0;
}

uint16_t filter_ident(int bw, int bh, int parts, uint8_t * data,
                      uint8_t * out) {
    int stat_solid = 0;
    int stat_ident = 0;
    int stat_fewer = 0;
    int stat_valid = 0;
    int bs = bw * bh;
    for (int i = 0; i < 1024; i++) {
        uint8_t * cand = data + i * bs;

        uint8_t norm_cand[bs];

        switch (reorder_blk(bs, parts, cand, norm_cand)) {
        case 1:
            stat_solid++;
            out[i] = 0;
            continue;
        case 2:
            stat_fewer++;
            out[i] = 0;
            continue;
        default:
            break;
        }

        /* Whether to consider this candidate during partition selection */
        uint8_t flag = 1;

        for (int j = i - 1; j >= 0; j--) {

            if (!out[j])
                continue;

            uint8_t * cmp = data + j * bs;
            uint8_t norm_cmp[bs];

            if (reorder_blk(bs, parts, cmp, norm_cmp) != 0)
                continue;

            uint8_t k;
            for (k = 0; k < bs; k++) {
                if (norm_cmp[k] != norm_cand[k])
                    break;
            }

            if (k == bs) {
                stat_ident++;
                flag = 0;
                break;
            }
        }

        out[i] = flag;
        if (out[i])
            stat_valid++;
    }
    if (enable_print)
        printf("Block: %dx%d, parts: %d, valid: %d, solid: %d, fewer: %d, "
               "ident: %d\n",
               bw, bh, parts, stat_valid, stat_solid, stat_fewer, stat_ident);

    return stat_valid;
}

int main(int argc, char ** argv) {

    uint8_t * incl = calloc(1024, 1);
    if (!incl)
        return ENOMEM;

    FILE * f = fopen("partition.h", "w");
    if (!f)
        return EIO;

    enable_print = argc == 1;

    fprintf(f, "/* Automatically generated code. Do not edit! */\n"
               "#ifndef MPASTC_PARTITION_H_\n"
               "#define MPASTC_PARTITION_H_\n\n"
               "#ifdef __cplusplus\n"
               "extern \"C\" {\n"
               "#endif\n\n"
               "#include <stdint.h>\n\n");

    for (int np = 0; np < n_parts; np++) {

        for (int b = 0; b < n_sizes; b++) {
            uint16_t n_valid_ctr = 0;
            uint8_t bw = block_sizes[b][0];
            uint8_t bh = block_sizes[b][1];
            uint8_t part_count = parts[np];
            uint8_t * part_data = malloc(bw * bh * 1024);
            if (!part_data)
                return ENOMEM;

            gen_part(bw, bh, part_count, part_data);
            uint16_t n_valid =
                filter_ident(bw, bh, part_count, part_data, incl);
            uint16_t * part_reverse_map = calloc(1024, 2);

            /* The reverse mapping of consecutive internal IDs -> actual
             * partition ID */
            fprintf(f, "const uint16_t part_reverse_%dx%d_%d[%d] = {", bw, bh,
                    part_count, n_valid);
            for (int i = 0; i < 1024; i++) {
                if (incl[i]) {
                    part_reverse_map[n_valid_ctr++] = i;
                    fprintf(f, "%d%s", i,
                            n_valid_ctr == n_valid ? "};\n" : ",");
                }
            }

            /* The number of valid partitions for this block/partition count */
            fprintf(f, "const uint16_t part_valid_count_%dx%d_%d = %d;\n", bw,
                    bh, part_count, n_valid);

            // /* Include/exclude mask data */
            // fprintf(f, "const uint8_t part_mask_%dx%d_%d[%d] = {", bw, bh,
            //         part_count, n_valid);
            // for (int i = 0; i < 1024; i++) {
            //     fprintf(f, "%d%s", incl[i], i == 1023 ? "};\n" : ",");
            // }

            n_valid_ctr = 0;
            fprintf(f, "const uint8_t part_data_%dx%d_%d[%d] = {", bw, bh,
                    part_count, bw * bh * n_valid);
            for (int i = 0; i < n_valid; i++) {
                uint16_t pid = part_reverse_map[i];
                for (int j = 0; j < bw * bh; j++) {
                    uint8_t * base = part_data + pid * bw * bh;

                    uint8_t sum = 0, seen[4] = {0, 0, 0, 0};
                    for (int i = 0; i < bw * bh; i++) {
                        seen[base[i]] = 1;
                    }
                    for (int i = 0; i < 4; i++) {
                        sum += seen[i];
                    }
                    if (sum != part_count) {
                        printf("Error in %d-pid %d -> %d\n", part_count, i, pid);
                        return 1;
                    }

                    fprintf(f, "%d%s", base[j],
                            (i == n_valid - 1 && j == bw * bh - 1) ? "};\n"
                                                                   : ",");
                }
            }

            free(part_data);
        }
    }
    free(incl);

    fprintf(f, "\nstatic uint16_t get_part_count(int bw, int bh, int parts)"
               " { \n");
    for (int np = 0; np < n_parts; np++) {
        fprintf(f, "    if (parts == %d) {\n", parts[np]);
        for (int b = 0; b < n_sizes; b++) {
            uint8_t bw = block_sizes[b][0];
            uint8_t bh = block_sizes[b][1];
            fprintf(f,
                    "        if (bw == %d && bh == %d)\n"
                    "            return part_valid_count_%dx%d_%d;\n",
                    bw, bh, bw, bh, parts[np]);
        }
        fprintf(f, "    }\n");
    }
    fprintf(f, "    return 0;\n}\n");

    fprintf(f, "\nstatic const uint16_t * get_part_mapping(int bw, int bh, int "
               "parts) {\n");
    for (int np = 0; np < n_parts; np++) {
        fprintf(f, "    if (parts == %d) {\n", parts[np]);
        for (int b = 0; b < n_sizes; b++) {
            uint8_t bw = block_sizes[b][0];
            uint8_t bh = block_sizes[b][1];
            fprintf(f,
                    "        if (bw == %d && bh == %d)\n"
                    "            return part_reverse_%dx%d_%d;\n",
                    bw, bh, bw, bh, parts[np]);
        }
        fprintf(f, "    }\n");
    }
    fprintf(f, "    return 0;\n}\n");

    fprintf(f, "\nstatic const uint8_t * get_part_data(int bw, int bh, int "
               "parts) {\n");
    for (int np = 0; np < n_parts; np++) {
        fprintf(f, "    if (parts == %d) {\n", parts[np]);
        for (int b = 0; b < n_sizes; b++) {
            uint8_t bw = block_sizes[b][0];
            uint8_t bh = block_sizes[b][1];
            fprintf(f,
                    "        if (bw == %d && bh == %d)\n"
                    "            return part_data_%dx%d_%d;\n",
                    bw, bh, bw, bh, parts[np]);
        }
        fprintf(f, "    }\n");
    }
    fprintf(f, "    return 0;\n"
               "}\n\n"
               "#ifdef __cplusplus\n"
               "}\n"
               "#endif\n\n"
               "#endif\n");
    fclose(f);
    return 0;
}