/* This version bypasses the encoding quality selection and chooses a single
 * quantization level. Currently this is used for ASTC output, since there are
 * some issues with BISE encoding. */

#ifndef MPASTC_ENCCOMB_SINGLE_H_
#define MPASTC_ENCCOMB_SINGLE_H_

#include "astc.h"
#include "astcenc_interop.h"
#include <stdint.h>

#ifndef ENC_COLOR
#define ENC_COLOR 0
#define ENC_COLOUR ENC_COLOR
#endif

#ifndef ENC_WEIGHT
#define ENC_WEIGHT 1
#endif

const uint8_t enc_s_table_4x4_1[] = {QUANT_32, QUANT_32, 255, 255};
const uint8_t enc_s_avail_4x4_1 = 1;
const uint8_t enc_s_table_4x4_2[] = {QUANT_16, QUANT_8, 255, 255};
const uint8_t enc_s_avail_4x4_2 = 1;
const uint8_t enc_s_table_4x4_3[] = {QUANT_16, QUANT_3, 255, 255};
const uint8_t enc_s_avail_4x4_3 = 5;
const uint8_t enc_s_table_4x4_4[] = {QUANT_8, QUANT_3, 255, 255};
const uint8_t enc_s_avail_4x4_4 = 2;

/* Get all possible combinations of colour and weight bit allocations.
 * Returns an array consisting of color, weight configurations, repeated.
 * The end of the array is signaled by the value 255. */
static const uint8_t * get_enc_combination_single(uint8_t parts) {
    switch (parts) {
    case 1:
        return enc_s_table_4x4_1;
    case 2:
        return enc_s_table_4x4_2;
    case 3:
        return enc_s_table_4x4_3;
    case 4:
        return enc_s_table_4x4_4;
    default:
        return 0;
    }
}

#endif