#include <errno.h>
#include <stdint.h>
#include <stdio.h>

#include "astcenc_interop.h"
#include <array>

int enable_print;

#define COLOR_MAX_QUANT QUANT_256
#define WEIGHT_MAX_QUANT QUANT_32
#define COLOR_MIN_QUANT QUANT_6
#define WEIGHT_MIN_QUANT QUANT_2

/* This is the table from astcenc for encoding */

struct btq_count {
    /** @brief The number of bits. */
    uint8_t bits : 6;

    /** @brief The number of trits. */
    uint8_t trits : 1;

    /** @brief The number of quints. */
    uint8_t quints : 1;
};

static const std::array<btq_count, 21> btq_counts{{
    {1, 0, 0}, // QUANT_2
    {0, 1, 0}, // QUANT_3
    {2, 0, 0}, // QUANT_4
    {0, 0, 1}, // QUANT_5
    {1, 1, 0}, // QUANT_6
    {3, 0, 0}, // QUANT_8
    {1, 0, 1}, // QUANT_10
    {2, 1, 0}, // QUANT_12
    {4, 0, 0}, // QUANT_16
    {2, 0, 1}, // QUANT_20
    {3, 1, 0}, // QUANT_24
    {5, 0, 0}, // QUANT_32
    {3, 0, 1}, // QUANT_40
    {4, 1, 0}, // QUANT_48
    {6, 0, 0}, // QUANT_64
    {4, 0, 1}, // QUANT_80
    {5, 1, 0}, // QUANT_96
    {7, 0, 0}, // QUANT_128
    {5, 0, 1}, // QUANT_160
    {6, 1, 0}, // QUANT_192
    {8, 0, 0}  // QUANT_256
}};

const char * range_to_str[] = {
    "QUANT_2",   "QUANT_3",   "QUANT_4",   "QUANT_5",  "QUANT_6",  "QUANT_8",
    "QUANT_10",  "QUANT_12",  "QUANT_16",  "QUANT_20", "QUANT_24", "QUANT_32",
    "QUANT_40",  "QUANT_48",  "QUANT_64",  "QUANT_80", "QUANT_96", "QUANT_128",
    "QUANT_160", "QUANT_192", "QUANT_256", 0};

/* Cumulative number of bits required to store n trits */
const uint8_t trits_bitcount[] = {0, 2, 4, 5, 7, 8};

/* Cumulative number of bits required to store n quints */
const uint8_t quints_bitcount[] = {0, 3, 5, 7};

uint64_t calc_bitcount(quant_method r, uint32_t n_values) {
    uint8_t bits = btq_counts[r].bits;
    uint8_t trits = btq_counts[r].trits;
    uint8_t quints = btq_counts[r].quints;

    uint64_t bitcount = 0;
    if (bits)
        bitcount += bits * n_values;
    if (trits)
        bitcount += n_values / 5 * 8 + trits_bitcount[n_values % 5];
    if (quints)
        bitcount += n_values / 3 * 7 + quints_bitcount[n_values % 3];
    return bitcount;
}

/* Available bits for encoding colour and texel data. Currently since we only
 * support the RGB LDR CEM and no dual-plane mode, only the partition count
 * affects the bits available for encoding data. */
static inline uint8_t get_avail_enc_bits(uint8_t partitions, uint8_t cem,
                                         uint8_t dp) {
    switch (partitions) {
    case 1:
        // 128 - 11 (block mode) - 2 (part) - 4 (CEM)
        return 111;
    case 2:
    case 3:
    case 4:
        // 128 - 11 (block mode) - 2 (part) - 10 (part#) - 6 (CEM)
        return 99;
    default:
        return 0;
    }
}

int main(int argc, char ** argv) {

    enable_print = argc == 1;

    FILE * f;
    f = fopen("enccomb.h", "w");
    if (!f)
        return errno;

    fprintf(f, "#ifndef MPASTC_ENCCOMB_H_\n"
               "#define MPASTC_ENCCOMB_H_\n"
               "\n"
               "#include \"astc.h\"\n"
               "#include <stdint.h>\n"
               "#include \"astcenc_interop.h\"\n"
               "\n"
               "#define ENC_COLOR 0\n"
               "#define ENC_COLOUR ENC_COLOR\n"
               "#define ENC_WEIGHT 1\n"
               "\n");

    int bs = 16; // 4 x 4 block
    for (int parts = 1; parts <= 4; parts++) {

        int ecount = 0;
        int8_t col_prec = COLOR_MAX_QUANT;
        int8_t wt_prec = WEIGHT_MAX_QUANT;
        int8_t prev_col_prec = COLOR_MAX_QUANT + 1;
        uint8_t avail_bits = get_avail_enc_bits(parts, 8, 0);

        fprintf(f, "const uint8_t enc_table_4x4_%d[] = {", parts);
        if (enable_print)
            printf("%d Partition%s: %d bits\n", parts, parts > 1 ? "s" : "",
                   avail_bits);
        if (enable_print)
            printf("    Colour    Bits Weight    Bits Total Waste\n");

        for (; wt_prec >= 0; wt_prec--) {
            /* Reduce precision until both weight and colour bits fit into
             * allowed space */
            uint8_t flag = 0;
            int8_t col_prec;
            uint64_t co_bits;
            uint64_t wt_bits = calc_bitcount((quant_method)wt_prec, bs);
            for (col_prec = COLOR_MAX_QUANT; col_prec > COLOR_MIN_QUANT;
                 col_prec--) {
                co_bits = calc_bitcount((quant_method)col_prec, 6 * parts);
                if (wt_bits + co_bits <= avail_bits) {
                    flag = 1;
                    break;
                }
            }

            // Could not find a suitable encoding with this level of precision
            if (!flag)
                continue;

            // The accuracy level did not change, ignore this encoding
            if (prev_col_prec == col_prec)
                continue;

            /* Print the current accuracy level */
            if (enable_print)
                printf("    %-9s %4d %-9s %4d %5d %5d\n",
                       range_to_str[col_prec], co_bits, range_to_str[wt_prec],
                       wt_bits, co_bits + wt_bits,
                       avail_bits - (co_bits + wt_bits));
            fprintf(f, "%s%s,%s",
                    prev_col_prec == COLOR_MAX_QUANT + 1 ? "" : ",",
                    range_to_str[col_prec], range_to_str[wt_prec]);

            prev_col_prec = col_prec;
            ecount++;
        }

        fprintf(f, ",255,255};\nconst uint8_t enc_avail_4x4_%d = %d;\n", parts,
                ecount);
        if (enable_print)
            printf("    = %d encodings available\n", ecount);
    }

    fprintf(f, "\n"
               "/* Get all possible combinations of colour and weight bit "
               "allocations.\n"
               " * Returns an array consisting of color, weight "
               "configurations, repeated.\n"
               " * The end of the array is signaled by the value 255. */\n"
               "static const uint8_t * get_enc_combinations(uint8_t parts) {\n"
               "    switch (parts) {\n");

    for (int p = 1; p <= 4; p++) {
        fprintf(f,
                "        case %d:\n"
                "            return enc_table_4x4_%d;\n",
                p, p);
    }
    fprintf(f, "        default:\n"
               "            return 0;\n"
               "    }\n"
               "}\n");

    fprintf(f,
            "\n"
            "static const uint8_t get_num_enc_combinations(uint8_t parts) {\n"
            "    switch (parts) {\n");
    for (int p = 1; p <= 4; p++) {
        fprintf(f,
                "        case %d:\n"
                "            return enc_avail_4x4_%d;\n",
                p, p);
    }
    fprintf(f, "        default:\n"
               "            return 0;\n"
               "    }\n"
               "}\n");

    fprintf(f, "\n#endif\n");
    return 0;
}