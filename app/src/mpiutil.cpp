#include "mpiutil.h"

int get_thread_count() {
    int nt = 0;
    #pragma omp parallel
    {
        if (omp_get_thread_num() == 0)
            nt = omp_get_num_threads();
    }
    return nt;
}

int init_mpi(int * rank, int * size) {

    int ret;

    ret = MPI_Init(NULL, NULL);
    if (ret != 0) {
        fprintf(stderr, "MPI Init failed\n");
        return ret;
    }

    ret = MPI_Comm_rank(MPI_COMM_WORLD, rank);
    if (ret != 0) {
        fprintf(stderr, "Could not determine rank\n");
        return ret;
    }
    ret = MPI_Comm_size(MPI_COMM_WORLD, size);
    if (ret != 0) {
        fprintf(stderr, "Could not determine communicator size\n");
        return ret;
    }

    if (*rank == 0)
        log_debug("Rank count: %d", *size);

    return 0;
}

int finalize_mpi() { return MPI_Finalize(); }

int32_t max_value(int32_t * vals, size_t len) {
    int32_t cur_max = INT32_MIN;
    for (size_t i = 0; i < len; i++) {
        if (vals[i] > cur_max)
            cur_max = vals[i];
    }
    return cur_max;
}

int32_t min_completed(int32_t * vals, size_t len) {
    int32_t cur_min = INT32_MAX;
    for (size_t i = 0; i < len; i++) {
        if (vals[i] >= 0 && vals[i] < cur_min) {
            cur_min = vals[i];
        }
    }
    return --cur_min;
}
