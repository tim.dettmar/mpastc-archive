#include "controller.h"
#include <queue>

int controller(void * buf, void * out, struct local_worker_opts * ctrl,
               uint32_t meta[META_MAX], int rank, int worldsize, int nthreads) {
    const int w = meta[META_WIDTH];
    const int h = meta[META_HEIGHT];
    const int bw = meta[META_BLK_W];
    const int bh = meta[META_BLK_H];
    const int bs = bw * bh;
    const int disp = meta[META_DISPATCH];
    const int str = meta[META_STRIDE];
    const int disp_bytes = disp * str;
    const int comp_size =
        ctrl->opts->test_run ? w * h * 3 : get_comp_size_astc(w, disp, bw, bh);
    int num_batches = h / disp;
    char err[MPI_MAX_ERROR_STRING];
    int err_len;
    int ret;

    ctrl->nthreads = nthreads;
    ctrl->meta = meta;

    int dtags[2] = {MPA_TAG_DATA_A, MPA_TAG_DATA_B};
    MPI_Request sreqs[worldsize * 2]; // Active sends
    MPI_Request rreqs[worldsize * 2]; // Active receives
    uint32_t map[worldsize * 2]; // Mapping of rank -> list index (double buf)
    uint8_t sact[worldsize * 2]; // Active sends tracker
    uint8_t ract[worldsize * 2]; // Active recv tracker
    int sleft = num_batches;     // Remaining sends
    int rleft = num_batches;     // Remaining receives
    int done = 0;                // Completed batches
    int spend = 0;               // Pending sends
    int rpend = 0;               // Pending receives
    memset(map, 0, sizeof(uint32_t) * worldsize * 2);
    int32_t tail = 0;     // the first free index of the list owo

    memset(sreqs, 0, sizeof(MPI_Request) * worldsize * 2);
    memset(rreqs, 0, sizeof(MPI_Request) * worldsize * 2);
    memset(map, 0, sizeof(uint32_t) * worldsize * 2);
    memset(sact, 0, worldsize * 2);
    memset(ract, 0, worldsize * 2);
    for (int i = 0; i < worldsize * 2; i++) {
        sreqs[i] = MPI_REQUEST_NULL;
        rreqs[i] = MPI_REQUEST_NULL;
    }

    log_debug("Batch count: %d, uncompressed %d bytes, compressed %d bytes (%d "
              "ASTC blocks), ratio: %.2f:1",
              num_batches, disp_bytes, comp_size, comp_size / 16,
              ((double)disp_bytes / (double)comp_size));

    /* Send image metadata */
    ret = MPI_Bcast((void *)meta, META_MAX, MPI_UINT32_T, 0, MPI_COMM_WORLD);
    if (ret != MPI_SUCCESS) {
        MPI_Error_string(ret, err, &err_len);
        log_error("Controller failed to broadcast metadata: %s", err);
        return ECOMM;
    }

    const int dst_base = ctrl->opts->use_mpi == 2 ? 0 : 1;
    const int q_len = worldsize * 2;
    while (1) {

        int idx = 0, flag = 0;

        /* Dispatch tasks locally. The local worker doesn't use double buffering
         * as there is nothing *to* buffer */
        if (ctrl->opts->use_mpi != 2 &&
            ctrl->counter.load(std::memory_order_acquire) % 2 == 0) {

            /* Done, because the thread relinquished control by incrementing the
             * counter. */
            if (sact[0]) {
                sact[0] = 0;
                done++;
            }

            if (sleft > 0) {
                map[0] = tail++;
                ctrl->in_buf =
                    (void *)get_row_data((uint8_t *)buf, str, disp, map[0]);
                ctrl->out_buf = (void *)((uint8_t *)buf + comp_size * map[0]);
                ctrl->counter.fetch_add(1, std::memory_order_acq_rel);
                sact[0] = 1;
                sleft--;
                rleft--;
                if (ctrl->opts->trace_steps)
                    log_trace("Send request 0 queued, idx: %d", map[0]);
            }
        }

        for (int dst = dst_base; dst < worldsize; dst++) {

            /* Dispatch tasks */
            if (sleft > 0) {
                for (int pg = 0; pg < 2; pg++) {
                    /* Fill the front buffer on other nodes first before
                     * resorting to back buffers */
                    if (pg == 1 && spend < worldsize - dst_base)
                        break;

                    const int s_idx = dst * 2 + pg;
                    if (!sact[s_idx] && !ract[s_idx]) {
                        map[s_idx] = tail++;
                        void * sbuf = (void *)get_row_data((uint8_t *)buf, str,
                                                           disp, map[s_idx]);
                        assert(sbuf);
                        ret =
                            MPI_Isend(sbuf, disp_bytes, MPI_BYTE, dst,
                                      dtags[pg], MPI_COMM_WORLD, &sreqs[s_idx]);
                        if (ret != MPI_SUCCESS) {
                            MPI_Error_string(ret, err, &err_len);
                            log_error("Controller failed to queue send to rank "
                                      "%d: %s",
                                      dst, err);
                            return ECOMM;
                        }
                        if (ctrl->opts->trace_steps) {
                            log_trace("Queued send idx %d -> rank %d pg %d",
                                      tail, dst, pg);
                        }
                        spend++;
                        sleft--;
                        sact[s_idx] = 1;
                    }
                }
            }

            /* Queue receives */
            if (rleft > 0) {
                for (int pg = 0; pg < 2; pg++) {
                    const int r_idx = dst * 2 + pg;
                    if (sact[r_idx] && !ract[r_idx]) {
                        void * rbuf = (uint8_t *)out + comp_size * map[r_idx];
                        assert(rbuf);
                        ret =
                            MPI_Irecv(rbuf, comp_size, MPI_BYTE, dst, dtags[pg],
                                      MPI_COMM_WORLD, &rreqs[r_idx]);
                        if (ret != MPI_SUCCESS) {
                            MPI_Error_string(ret, err, &err_len);
                            log_error("Controller failed to queue recv for "
                                      "rank %d: %s",
                                      dst, err);
                            return ECOMM;
                        }
                        rpend++;
                        rleft--;
                        ract[r_idx] = 1;
                        if (ctrl->opts->trace_steps) {
                            log_trace("Queued recv idx %d -> rank %d pg %d",
                                      map[r_idx], dst, pg);
                        }
                    }
                }
            }

            /* Check sends */
            ret = MPI_Testany(q_len, sreqs, &idx, &flag,
                              MPI_STATUS_IGNORE);
            if (ret != MPI_SUCCESS) {
                MPI_Error_string(ret, err, &err_len);
                log_error("Controller failed to check sends: %s", dst, err);
                return ECOMM;
            }
            if (flag && idx != MPI_UNDEFINED) {
                if (ctrl->opts->trace_steps) {
                    log_trace("Send complete idx %d -> rank %d pg %d", map[idx],
                              idx / 2, idx % 2);
                }
                sact[idx] = 0;
                sreqs[idx] = MPI_REQUEST_NULL;
                spend--;
            }

            /* Check receives */
            ret = MPI_Testany(q_len, rreqs, &idx, &flag,
                              MPI_STATUS_IGNORE);
            if (ret != MPI_SUCCESS) {
                MPI_Error_string(ret, err, &err_len);
                log_error("Controller failed to check sends: %s", dst, err);
                return ECOMM;
            }
            if (flag && idx != MPI_UNDEFINED) {
                if (ctrl->opts->trace_steps) {
                    log_trace("Recv complete idx %d -> rank %d pg %d", map[idx],
                              idx / 2, idx % 2);
                }
                ract[idx] = 0;
                rreqs[idx] = MPI_REQUEST_NULL;
                rpend--;
                done++;
            }
        }

        if (done == num_batches)
            break;
    }

    if (ctrl->opts->use_mpi != 2)
        ctrl->counter.store(-1, std::memory_order_release);

    for (int i = dst_base; i < worldsize; i++) {
        uint8_t end = 1;
        ret = MPI_Isend(&end, 1, MPI_BYTE, i, MPA_TAG_CTRL, MPI_COMM_WORLD,
                        &sreqs[i]);
        if (ret != MPI_SUCCESS) {
            MPI_Error_string(ret, err, &err_len);
            log_error("Failed to send stop signal to rank %d: %s", i, err);
            return ECOMM;
        }
    }

    int s = ctrl->opts->use_mpi == 2 ? worldsize : worldsize - 1;
    ret = MPI_Waitall(s, &sreqs[dst_base], MPI_STATUS_IGNORE);
    if (ret != MPI_SUCCESS) {
        MPI_Error_string(ret, err, &err_len);
        log_error("Failed to wait on stop signals: %s", err);
        return ECOMM;
    }

    return 0;
}