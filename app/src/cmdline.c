#include "cmdline.h"
#include "log.h"

#ifdef __cplusplus
extern "C" {
#endif

/* The arguments in app_opts and opt_desc must be in the same order. They must
 * also have all zeros as the last element to signal the end of the array. */

static struct option app_opts[] = {{"mpi", no_argument, NULL, 'm'},
                                   {"part", no_argument, NULL, 'p'},
                                   {"gui", no_argument, NULL, 'g'},
                                   {"file", required_argument, NULL, 'f'},
                                   {"output", required_argument, NULL, 'o'},
                                   {"dry_run", no_argument, NULL, 'd'},
                                   {"bench", no_argument, NULL, 'b'},
                                   {"raw", no_argument, NULL, 'r'},
                                   {"verbose", no_argument, NULL, 'v'},
                                   {"threads", required_argument, NULL, 't'},
                                   {"size", required_argument, NULL, 's'},
                                   {"help", no_argument, NULL, 'h'},
                                   {0, 0, 0, 0}};

struct app_opt_desc {
    int id;
    char * help_str;
};

static struct app_opt_desc opt_desc[] = {
    {'m', "Enable MPI support"},
    {'p', "Enable partitioning mode"},
    {'g', "Enable visualization (requires display server)"},
    {'f', "The file to load for compression"},
    {'o', "Output file. If not specified, only compression stats displayed"},
    {'d', "Dry run - compress and display the result without encoding"},
    {'b', "Benchmark mode - suppress all output, except results"},
    {'r', "Output a raw texture without the ASTC header"},
    {'v', "Trace individual compression steps. Warning: verbose output"},
    {'t', "Number of threads per node"},
    {'s', "Work group dispatch size multiplier"},
    {'h', "Display this help message"},
    {0, 0}};

void print_help() {
    printf("mpASTC Compressor Utility\n"
           "Copyright (c) 2023 Tim Dettmar\n"
           "\n"
           "Usage:\n"
           "\n");

    struct app_opt_desc * desc = opt_desc;
    struct option * opts = app_opts;
    while (desc->id) {
        printf("    -%c, --%s: %s\n", opts->val, opts->name, desc->help_str);
        opts++;
        desc++;
    }
    printf("\n");
}

int load_opts(int argc_, char ** argv_, struct mpa_options * out) {
    memset(out, 0, sizeof(*out));
    int o, optidx = -1;
    while ((o = getopt_long(argc_, argv_, "mpghbdrvs:t:f:", app_opts,
                            &optidx)) != -1) {
        if (optidx >= 0) {
            o = app_opts[optidx].val;
            optidx = -1;
        }
        switch (o) {
        case 'm':
            out->use_mpi = 1;
            break;
        case 'p':
            out->partitioning = 1;
            break;
        case 'g':
            out->use_gui = 1;
            break;
        case 'f':
            out->filename = optarg;
            break;
        case 'v':
            out->trace_steps = 1;
            break;
        case 'h':
            if (!out->help_req)
                print_help();
            out->help_req = 1;
            break;
        case 't':
            if (out->use_mpi != 2) {
                out->threads = atoi(optarg);
                if (out->threads == 0) {
                    log_error("Invalid thread count specified: %s", optarg);
                    return EINVAL;
                }
            }
            break;
        case 's':
            out->batch_size = atoi(optarg);
            if (out->batch_size == 0) {
                log_error("Invalid batch size specified: %s", optarg);
                return EINVAL;
            }
            break;
        case 'o':
            out->out_filename = optarg;
            break;
        case 'r':
            out->raw_output = 1;
            break;
        case 'd':
            out->test_run = 1;
            break;
        case 'b':
            out->bench = 1;
            break;
        case ':':
            log_error("Missing parameter for option %s", opt_desc[optidx]);
            return EINVAL;
        case '?':
        default:
            log_warn("Invalid command-line option at index %d", optidx);
            break;
        }
    }

    /* mpastc can perform the calculations for it, but it doesn't currently
     * support partitioned output in ASTC format. */
    if (!out->test_run)
        out->partitioning = 0;

    return 0;
}

#ifdef __cplusplus
}
#endif