#include "visual.h"

static void glfw_error_callback(int error, const char * description) {
    fprintf(stderr, "GLFW Error %d: %s\n", error, description);
}

int gui_load_texture(visual_window * ctx, void * buf, uint32_t width,
                     uint32_t height, GLuint format, visual_target target) {
    /* Load the texture */
    glGenTextures(1, &ctx->tex_ids[target]);
    glBindTexture(GL_TEXTURE_2D, ctx->tex_ids[target]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB,
                 GL_UNSIGNED_BYTE, buf);

    /* Set scaling parameters */
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    return 0;
}

void * gui_thread(void * arg) {
    visual_window * vis = (visual_window *)arg;

    glfwSetErrorCallback(glfw_error_callback);
    if (!glfwInit())
        return 0;

    const char * glsl_version = "#version 130";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    // glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+
    // only glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // 3.0+ only

    vis->window = glfwCreateWindow(vis->width, vis->height,
                                   "mpASTC Visualization Window", NULL, NULL);
    if (!vis->window) {
        glfwTerminate();
        return 0;
    }

    glfwMakeContextCurrent(vis->window);
    glfwSwapInterval(1);

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    vis->io = ImGui::GetIO();
    vis->io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(vis->window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    pthread_mutex_unlock(&vis->mut); // init complete

    bool demo = true;
    while (!glfwWindowShouldClose(vis->window)) {
        glfwPollEvents();

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        if (vis->cmd) {
            pthread_mutex_lock(&vis->mut);
            gui_load_texture(vis, vis->cmd->buf, vis->cmd->width,
                             vis->cmd->height, vis->cmd->format,
                             vis->cmd->target);
            vis->tex_w = vis->cmd->width;
            vis->tex_h = vis->cmd->height;
            vis->cmd = 0;
            pthread_mutex_unlock(&vis->mut);
        }

        if (vis->flags & VIS_SHOW_RAW_TEX) {
            ImGui::Begin("Raw texture data");
            ImGui::Text("Size: %d x %d", vis->tex_w, vis->tex_h);
            ImGui::Image((void *)(intptr_t)vis->tex_ids[TARGET_RAW],
                         ImVec2(vis->tex_w, vis->tex_h));
            ImGui::End();
        }

        if (demo)
            ImGui::ShowDemoWindow(&demo);

        ImGui::Render();
        int w, h;
        glfwGetFramebufferSize(vis->window, &w, &h);
        glViewport(0, 0, w, h);
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        glfwSwapBuffers(vis->window);
    }
    return 0;
}

int create_window(visual_window * out) {
    out->flags = 0;
    out->tex_w = 0;
    out->tex_h = 0;
    out->cmd = 0;
    pthread_mutex_init(&out->mut, NULL);
    pthread_mutex_lock(&out->mut);
    pthread_create(&out->thread, NULL, gui_thread, out);
    pthread_mutex_lock(&out->mut);
    pthread_mutex_unlock(&out->mut);
    return 0;
}

int render_texture(visual_window * ctx, void * tex, uint8_t type,
                   uint32_t width, uint32_t height, uint32_t stride) {
    pthread_mutex_lock(&ctx->mut);
    cmd.buf = tex;
    cmd.format = GL_RGB;
    cmd.width = width;
    cmd.height = height;
    ctx->cmd = &cmd;
    ctx->flags |= VIS_SHOW_RAW_TEX;
    pthread_mutex_unlock(&ctx->mut);
    return 0;
}