#include "block.h"

void extract_block(uint8_t * raw, void * out, uint8_t convert, uint64_t stride,
                   int64_t x, int64_t y, uint8_t bw, uint8_t bh) {
    assert(x % bw == 0);
    assert(y % bh == 0);
    uint64_t pitch = 3; // 24 bit
    uint64_t iw = 0;
    for (uint64_t iy = y; iy < y + bh; iy++) {
        if (convert) {
            for (uint64_t ix = x; ix < x + bw; ix++) {
                uint8_t * ptr = raw + iy * stride + ix * pitch;
                ((ir_texel *)out)[iw].r = (ir_fmt)ptr[0];
                ((ir_texel *)out)[iw].g = (ir_fmt)ptr[1];
                ((ir_texel *)out)[iw].b = (ir_fmt)ptr[2];
                iw++;
            }
        } else {
            uint8_t * ptr = raw + iy * stride + x * pitch;
            memcpy(((uint8_t *)out) + iw, ptr, stride * bw);
            iw += stride * bw;
        }
    }
}