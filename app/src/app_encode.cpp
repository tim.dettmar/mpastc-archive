#include "app_encode.h"

int check_solid(ir_texel * buf, uint8_t bw, uint8_t bh) {
    uint8_t bs = bw * bh;
    uint8_t diff_count = 0;
    for (uint8_t i = 1; i < bs; i++) {
        if ((int)buf[i].r != (int)buf[0].r || (int)buf[i].g != (int)buf[0].g ||
            (int)buf[i].b != (int)buf[0].b)
            diff_count++;
    }
    return diff_count == 0;
}

void place_test_block(texel * src, texel * out, uint32_t id, uint8_t bw,
                      uint8_t bh, uint32_t width, uint32_t height) {
    uint32_t x, y;
    id_to_index(id, width, height, bh, &x, &y);
    int j = 0;
    for (uint32_t yt = y; yt < y + bh; yt++) {
        for (uint32_t xt = x; xt < x + bw; xt++) {
            texel * dst = out + width * yt + xt;
            *dst = src[j++];
        }
    }
}

void unpack_test_blocks(texel * texels, texel * out, uint8_t bw, uint8_t bh,
                        uint32_t width, uint32_t height) {
    int nblks = width * height / (bw * bh);
    for (int i = 0; i < nblks; i++) {
        texel * src = texels + i * 16;
        uint32_t x, y;
        id_to_index(i, width, bw, bh, &x, &y);
        int j = 0;
        for (uint32_t xt = x; xt < x + bw; xt++) {
            for (uint32_t yt = y; yt < y + bh; yt++) {
                texel * dst = out + width * yt + xt;
                *dst = src[j++];
            }
        }
    }
}

int encode_blocks(uint32_t meta[META_MAX], void * buf, void * out,
                  int n_threads) {

    const int bw = meta[META_BLK_W];
    const int bh = meta[META_BLK_H];
    const int bs = bw * bh;
    const int w = meta[META_WIDTH];
    const int h = meta[META_HEIGHT];
    int ret;

    if (n_threads == 0) {
#pragma omp parallel shared(n_threads)
        {
#pragma omp single
            { n_threads = omp_get_num_threads(); }
        }
    }

/* Compression stage */
#pragma omp parallel for num_threads(n_threads)
    for (uint32_t i = 0; i < meta[META_WIDTH] * meta[META_DISPATCH] / bs; i++) {

        float min_err = INFINITY;
        struct best_encoding be;
        ir_texel ir_data[bs];
        uint32_t x, y;
        uint8_t use_single = 0;
        uint8_t comb[2] = {255, 255};

        /* Upsample the texel data */
        id_to_index(i, w, bw, bh, &x, &y);
        extract_block((uint8_t *)buf, ir_data, 1, w * 3, x, y, bw, bh);

        /* Determine whether the block is a single colour and
         * use the fast path */
        if (check_solid(ir_data, bw, bh)) {
            ir_texel avg = ir_black;
            for (int j = 0; j < bs; j++) {
                avg = ir_texel_addv(avg, ir_data[j]);
            }
            avg = ir_texel_div(avg, bs);
            if (meta[META_FLAGS] & ENC_FLAG_INTERMEDIATE) {
                texel tmp[16];
                for (int j = 0; j < bs; j++) {
                    tmp[j].r = avg.r;
                    tmp[j].g = avg.g;
                    tmp[j].b = avg.b;
                }
                place_test_block(tmp, (texel *)out, i, bw, bh, meta[META_WIDTH],
                                 meta[META_DISPATCH]);
            } else {
                uint8_t * tmp_out = ((uint8_t *)out) + i * 16;
                encode_void_extent(avg, (void *)tmp_out);
            }
            continue;
        }

        /*  Determine single-partition error. Since it's the first iteration
           it will always be the current best */
        be.part_count = 1;
        ret = find_single_partition(bw, bh, ir_data, &be.err, be.eps, be.wts,
                                    be.ranges);
        if (ret < 0) {
            log_fatal("Error on thread %d: %s", omp_get_thread_num(),
                      strerror(-ret));
            assert(false && "Single-partition search failed");
        }

        /* This case should normally have been caught by the check for solid
         * blocks but there are specific circumstances e.g. texels 0,1,0 and
         * 1,0,0 where this codepath is needed */
        if (ret == 1) {
            ir_texel avg = ir_black;
            for (int j = 0; j < bs; j++) {
                avg = ir_texel_addv(avg, ir_data[j]);
            }
            avg = ir_texel_div(avg, bs);
            if (meta[META_FLAGS] & ENC_FLAG_INTERMEDIATE) {
                texel tmp[16];
                for (int j = 0; j < bs; j++) {
                    tmp[j].r = avg.r;
                    tmp[j].g = avg.g;
                    tmp[j].b = avg.b;
                }
                place_test_block(tmp, (texel *)out, i, bw, bh, meta[META_WIDTH],
                                 meta[META_DISPATCH]);
            } else {
                uint8_t * tmp_out = ((uint8_t *)out) + i * 16;
                encode_void_extent(avg, (void *)tmp_out);
            }
            continue;
        }

        min_err = be.err;

        if (meta[META_FLAGS] & ENC_FLAG_PARTITIONING) {
            for (int p = 2; p <= 4; p++) {
                
                ir_fmt err;
                uint8_t used_parts[4] = {0, 0, 0, 0};
                uint8_t has_part = 0;
                uint8_t clusters[bs];
                uint8_t * part_data = (uint8_t *)get_part_data(bw, bh, p);
                ir_endpoint peps[4];
                ir_weight pwts[bs];
                assert(part_data);

#ifndef NDEBUG
                /* Use invalid data so that the checks catch it when
                 * unintentionally used */
                for (int i = 0; i < bs; i++) {
                    clusters[i] = 254;
                    pwts[i].part = 254;
                    pwts[i].weight = NAN;
                }
                for (int i = 0; i < p; i++) {
                    peps[i].a = ir_texel_create(NAN, NAN, NAN);
                    peps[i].b = ir_texel_create(NAN, NAN, NAN);
                }
#endif

                // ret = kmeans(ir_data, NULL, p, bs, 10, clusters);
                // if (ret < 0) {
                //     log_fatal("Error on thread %d: %s", omp_get_thread_num(),
                //               strerror(-ret));
                //     assert(false && "K-Means clustering failed");
                // }

                /* Cluster texels and find closest matching partition */
                ret = kmeans_pp(ir_data, p, bs, 16, clusters);
                if (ret < 0) {
                    log_fatal("Error on thread %d: %s", omp_get_thread_num(),
                              strerror(-ret));
                    assert(false && "K-Means clustering failed");
                }

                for (int i = 0; i < bs; i++) {
                    if (!used_parts[clusters[i]]) {
                        used_parts[clusters[i]] = 1;
                        has_part++;
                    }
                }

                if (has_part == 1)
                    continue;

                int pid = find_closest_partition(bw, bh, p, clusters);
                if (pid < 0) {
                    log_fatal("Error on thread %d: %s", omp_get_thread_num(),
                              strerror(-ret));
                    assert(false && "Partition search failed");
                }

                /* Certain multipartition blocks have fewer partitions than
                 * expected. For instance, a 3-partition block may only have
                 * two partitions actually used in the partition mapping. We
                 * only need to actually compute the endpoints for the
                 * partitions actually in use. This should, however, have been
                 * caught by the code generator! */
                part_data = part_data + pid * bs;
                memset(&used_parts, 0, 4);
                has_part = 0;
                for (int i = 0; i < bs; i++) {
                    if (!used_parts[part_data[i]]) {
                        used_parts[part_data[i]] = 1;
                        has_part++;
                    }
                }

                if (has_part == 1) {
                    assert(false);
                    use_single++;
                    continue;
                }

                if (has_part != p) {
                    assert(false);
                    use_single++;
                    continue;
                }

                for (int pp = 0; pp < p; pp++) {
                    uint8_t mask[bs];
                    uint8_t used = 0;
                    uint8_t uidx = 0;
                    for (int i = 0; i < bs; i++) {
                        uint8_t val = part_data[i] == pp;
                        mask[i] = val;
                        used += val;
                        uidx = i;
                    }

                    if (used == 1) {
                        peps[pp].a = ir_data[uidx];
                        peps[pp].b = ir_data[uidx];
                        check_valid_ep(&peps[pp]);
                    } else {
                        ret = block_get_ep(ir_data, mask, bs, &peps[pp]);
                        check_valid_ep(&peps[pp]);
                    }
                }

                /* Calculate weights */
                for (int i = 0; i < bs; i++) {
                    pwts[i].weight =
                        ir_ep_calc_wt(peps[part_data[i]], ir_data[i]);
                    pwts[i].part = part_data[i];
                    check_valid_weight(&pwts[i]);
                }

                /* Error checking */
                {
                    uint8_t upart = 0;
                    for (int i = 0; i < bs; i++) {
                        upart |= (1 << part_data[i]);
                    }
                    if (upart != (1 << p) - 1) {
                        log_fatal("upart = %d, part = %d", upart, (1 << p) - 1);
                        assert(false);
                    }
                }

                /* Calculate ideal bit allocation */
                ret = find_best_encoding_comb(ir_data, peps, pwts, p, bs, &err,
                                              comb);
                if (ret < 0) {
                    log_fatal("Error on thread %d: %s", omp_get_thread_num(),
                              strerror(-ret));
                    assert(false && "Encoding combination calculation failed");
                }
                if (comb[ENC_COLOR] == 255 || comb[ENC_WEIGHT] == 255)
                    assert(false);

                /* If error is better, overwrite */
                if (err < min_err) {
                    memcpy(be.eps, peps, sizeof(ir_endpoint) * p);
                    memcpy(be.wts, pwts, sizeof(ir_weight) * bs);
                    memcpy(be.ranges, comb, sizeof(comb));
                    be.part_count = p;
                    be.part_id = get_part_mapping(bw, bh, p)[pid];
                    be.err = err;
                    min_err = err;
                }
            }
        }

        /* Error checking */
        uint8_t hdr, wt, col;
        uint64_t bc =
            get_bitcount(be.part_count, bs, (quant_method)be.ranges[ENC_WEIGHT],
                         (quant_method)be.ranges[ENC_COLOR], &hdr, &wt, &col);
        assert(bc <= 128);
        assert(be.ranges[ENC_COLOR] < QUANT_MAX &&
               be.ranges[ENC_WEIGHT] < QUANT_MAX);

        /* Debugging mode: interpret the intermediate representation and store
         * it without any encoding. This is used to verify the endpoint
         * selections are actually correct. */
        if (meta[META_FLAGS] & ENC_FLAG_INTERMEDIATE) {
            texel tmp[16];
            ir_texel_decode(be.eps, be.wts, bs, tmp);
            place_test_block(tmp, (texel *)out, i, bw, bh, meta[META_WIDTH],
                             meta[META_DISPATCH]);
        } else {
            /* Perform the block encoding */
            uint8_t * tmp_out = ((uint8_t *)out) + i * 16;
#if 0
            encode_astc_block(be.eps, be.wts, be.part_count, be.part_id,
                                (quant_method)be.ranges[ENC_WEIGHT],
                                (quant_method)be.ranges[ENC_COLOR], tmp_out);
#else
            uint64_t wt_bits = encode_weights(
                be.wts, be.eps, (quant_method)be.ranges[ENC_WEIGHT], tmp_out);
            int off = set_block_header(bw, bh, 0, be.part_count, be.part_id,
                                       (quant_method)be.ranges[ENC_WEIGHT], 0,
                                       tmp_out);
            assert(off > 0);
            off += encode_rgb_endpoints(be.eps, be.part_count,
                                        (quant_method)be.ranges[ENC_COLOR],
                                        (uint64_t)off, tmp_out);
            uint64_t zero = 0;
            // writebits((uint8_t *)&zero, tmp_out, off, 128 - bc);
            assert(off + wt_bits <= 128);
#endif
        }
    }
    return 0;
}
