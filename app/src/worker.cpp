#include "worker.h"

int worker(struct mpa_options * opts, int rank, int size) {
    int ret;
    /* Get metadata first */
    uint32_t meta[META_MAX];
    memset(meta, 0, sizeof(meta));
    char err[MPI_MAX_ERROR_STRING];
    int errl;
    int nt = 1;

    ret = MPI_Bcast(meta, META_MAX, MPA_META_TYPE, 0, MPI_COMM_WORLD);
    if (ret != MPI_SUCCESS) {
        MPI_Error_string(ret, err, &errl);
        log_error("Failed to receive metadata on rank %d: %s", rank, err);
        return ECOMM;
    }
    if (opts->trace_steps)
        log_debug("Received image metadata");

    if (meta[META_FLAGS] & ENC_FLAG_THREADING) {
        nt = get_thread_count();
    }

    if (meta[META_MAX_THR])
        nt = nt > (int) meta[META_MAX_THR] ? meta[META_MAX_THR] : nt;

    log_debug("[%d] Available threads: %d", rank, nt);

    size_t recv_size = meta[META_DISPATCH] * meta[META_STRIDE];
    size_t out_size =
        meta[META_FLAGS] & ENC_FLAG_INTERMEDIATE
            ? meta[META_DISPATCH] * meta[META_STRIDE]
            : get_comp_size_astc(meta[META_WIDTH], meta[META_DISPATCH],
                                 meta[META_BLK_W], meta[META_BLK_H]);
    void * buf = malloc(recv_size * 2);
    void * out = malloc(out_size * 2);
    if (!buf || !out) {
        log_fatal("Failed to allocate texture storage buffers!");
        return ENOMEM;
    }

    uint8_t ract[2] = {0, 0};
    uint8_t sact[2] = {0, 0};
    int dtags[2] = {MPA_TAG_DATA_A, MPA_TAG_DATA_B};
    MPI_Request rreqs[2];
    MPI_Request sreqs[2];
    uint8_t stop_buf;
    MPI_Request stop_req;
    MPI_Status stats[3];

    for (int i = 0; i < 2; i++) {
        sreqs[i] = MPI_REQUEST_NULL;
        rreqs[i] = MPI_REQUEST_NULL;
    }

    ret = MPI_Irecv(&stop_buf, 1, MPI_BYTE, 0, MPA_TAG_CTRL, MPI_COMM_WORLD,
                    &stop_req);
    if (ret != MPI_SUCCESS) {
        MPI_Error_string(ret, err, &errl);
        log_error("Failed to receive image data on rank %d: %s", rank, err);
        return ECOMM;
    }

    /* Main worker loop */
    while (1) {

        int flag = 0, idx = 0;
        /* Check for stop command */
        ret = MPI_Test(&stop_req, &flag, &stats[2]);
        if (ret != MPI_SUCCESS) {
            MPI_Error_string(ret, err, &errl);
            log_error("Failed to receive image data on rank %d: %s", rank, err);
            return ECOMM;
        }
        if (flag) {
            log_info("[%d] Return requested by root node", rank);
            break;
        }
        flag = 0;

        /* Queue receives */
        for (int i = 0; i < 2; i++) {
            if (!sact[i] && !ract[i]) {
                void * tmp = (void *)((uint8_t *)buf + recv_size * i);
                ret = MPI_Irecv(tmp, recv_size, MPI_BYTE, 0, dtags[i],
                                MPI_COMM_WORLD, &rreqs[i]);
                if (ret != MPI_SUCCESS) {
                    MPI_Error_string(ret, err, &errl);
                    log_error("Failed to receive image data on rank %d: %s",
                              rank, err);
                    return ECOMM;
                }
                if (meta[META_FLAGS] & ENC_FLAG_VERBOSE)
                    log_trace("[%d] Queued recv tag: %d", rank, dtags[i]);
                ract[i] = 1;
            }
        }

        /* Check receive status */
        ret = MPI_Testany(2, rreqs, &idx, &flag, stats);
        if (ret != MPI_SUCCESS) {
            MPI_Error_string(ret, err, &errl);
            log_error("Failed to receive image data on rank %d: %s", rank, err);
            return ECOMM;
        }
        if (flag && idx != MPI_UNDEFINED) {
            void * tmp_buf = (void *)((uint8_t *)buf + recv_size * idx);
            void * tmp_out = (void *)((uint8_t *)buf + out_size * idx);
            ret = encode_blocks(meta, tmp_buf, tmp_out, nt);
            if (ret != 0) {
                log_error("Failed to encode blocks: %s", strerror(ret));
                return ret;
            }
            ret = MPI_Isend(tmp_out, out_size, MPI_BYTE, 0, dtags[idx],
                            MPI_COMM_WORLD, &sreqs[idx]);
            if (ret != MPI_SUCCESS) {
                MPI_Error_string(ret, err, &errl);
                log_error("[%d] Failed to send image data: %s", rank,
                          err);
                return ECOMM;
            }
            if (meta[META_FLAGS] & ENC_FLAG_VERBOSE)
                    log_trace("[%d] Queued send tag: %d", rank, dtags[idx]);

            rreqs[idx] = MPI_REQUEST_NULL;
            ract[idx] = 0;
            sact[idx] = 1;
        }

        /* Check sends */
        ret = MPI_Testany(2, sreqs, &idx, &flag, stats);
        if (ret != MPI_SUCCESS) {
            MPI_Error_string(ret, err, &errl);
            log_error("[%d] Failed to send image data: %s", rank,
                          err);
            return ECOMM;
        }
        if (flag && idx != MPI_UNDEFINED) {
            if (meta[META_FLAGS] & ENC_FLAG_VERBOSE)
                    log_trace("[%d] Send complete tag: %d", rank, dtags[idx]);
            sact[idx] = 0;
            sreqs[idx] = MPI_REQUEST_NULL;
        }
    }

    free(buf);
    free(out);
    return 0;
}

void * worker_local(void * worker_opts) {
    struct local_worker_opts * wopts = (struct local_worker_opts *)worker_opts;
    struct mpa_options * opts = wopts->opts;
    int64_t counter = -1;
    int nt = 1, ret;

    if (wopts->meta[META_FLAGS] & ENC_FLAG_THREADING && !wopts->nthreads) {
        nt = get_thread_count();
    } else {
        nt = wopts->nthreads;
    }

    log_debug("[0] Worker threads: %d", nt);

    /* Counter even: controller active, odd: worker active */

    while (1) {
        while ((counter = wopts->counter.load(std::memory_order_acquire)) % 2 ==
               0)
            ;

        if (counter < 0) {
            log_info("[0] Return requested by control thread");
            break;
        }

        ret = encode_blocks(wopts->meta, wopts->in_buf, wopts->out_buf, nt);
        if (ret != 0) {
            log_error("Failed to encode blocks: %s", strerror(ret));
            return (void *)(uintptr_t)ret;
        }

        wopts->counter.fetch_add(1, std::memory_order_acq_rel);
    }

    return 0;
}
