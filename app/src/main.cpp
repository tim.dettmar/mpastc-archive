#include "log.h"
#include "texgen.h"
#include <assert.h>
#include <atomic>
#include <errno.h>
#include <pthread.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

#ifdef ENABLE_GUI
#include "visual.h"
#endif

#ifdef ENABLE_MPI
#include "controller.h"
#include "mpiutil.h"
#include "worker.h"
#endif

#include "app_encode.h"
#include "astc.h"
#include "bitwriter.h"
#include "block.h"
#include "cmdline.h"
#include "convert.h"
#include "interface.h"
#include "kmeans.h"
#include "part.h"
#include "partition.h"
#include "worker_opts.h"

struct astc_header {
    uint8_t magic[4];
    uint8_t block_x;
    uint8_t block_y;
    uint8_t block_z;
    uint8_t dim_x[3];
    uint8_t dim_y[3];
    uint8_t dim_z[3];
};

int rank, size;

float calc_seconds(struct timespec * start, struct timespec * end) {
    float s = ((float)start->tv_sec) + ((float)start->tv_nsec) / 1e9;
    float e = ((float)end->tv_sec) + ((float)end->tv_nsec) / 1e9;
    return e - s;
}

float conv_unit(float val, char * out) {
    if (val > 1e16) {
        *out = 'P';
        return val / 1e16;
    }
    if (val > 1e12) {
        *out = 'T';
        return val / 1e12;
    }
    if (val > 1e9) {
        *out = 'G';
        return val / 1e9;
    }
    if (val > 1e6) {
        *out = 'M';
        return val / 1e6;
    }
    if (val > 1e3) {
        *out = 'K';
        return val / 1e3;
    }
    *out = '\0';
    return val;
}

int main(int argc, char ** argv) {

    int rank = 0, size, ret;

    log_set_level(LOG_DEBUG);
    char strbuf[1024];
    uint32_t meta[META_MAX];

    struct mpa_options opts;
    if ((ret = load_opts(argc, argv, &opts)) != 0) {
        log_fatal("Error loading options: %s", strerror(ret));
        return -ret;
    }

    if (opts.trace_steps) {
        log_set_level(LOG_TRACE);
        part_verbose_log = 1;
        texel_verbose_log = 1;
        pca_verbose_log = 1;
    }

    if (opts.bench) {
        log_set_level(LOG_FATAL);
        part_verbose_log = 0;
        texel_verbose_log = 0;
        pca_verbose_log = 0;
    }

    if (opts.help_req) {
        return 0;
    }

#ifdef ENABLE_MPI
    if (opts.use_mpi) {
        ret = init_mpi(&rank, &size);
        if (ret != 0) {
            log_fatal("MPI init failed: %s", strerror(ret));
            return ret;
        }
        if (size == 1) {
            log_fatal("MPI support was requested, but only one rank is active");
            log_fatal("This configuration is unsupported");
            finalize_mpi();
            return EINVAL;
        }
    } else {
        log_info("MPI support is disabled. Processes started with mpirun will "
                 "perform the same task. Enable MPI support using -m");
        rank = 0;
        size = 1;
    }
#endif

    uint8_t bw = 4, bh = 4, bs = 16;
    int w, h, ch;
    texel * tex = 0;
    uint8_t * out = 0;
    if (rank == 0 && opts.filename) {
        log_info("Loading image %s", opts.filename);
        tex = (texel *)stbi_load(opts.filename, &w, &h, &ch, 3);
        if (!tex) {
            log_fatal("Could not load file: %s", opts.filename);
            return EINVAL;
        }
        if ((w % bw) || (h % bh)) {
            log_fatal("Width and/or height of provided image not a multiple of "
                      "the block size (%dx%d), cannot continue",
                      bw, bh);
            return EINVAL;
        }
        if (ch != 3) {
            log_fatal("Error: Image contains %d channels, unusable", ch);
            return EINVAL;
        }
        if (opts.test_run) {
            out = (uint8_t *)malloc(w * h * 3);
        } else {
            out = (uint8_t *)malloc(w * h);
        }
    } else if (rank == 0) {
        log_info("Using demo/test mode");
        /* Demo & integration testing mode */
        w = 128;
        h = 128;
        tex = (texel *)malloc(w * h * 3);
        if (opts.test_run) {
            out = (uint8_t *)malloc(w * h * 3);
        } else {
            out = (uint8_t *)malloc(w * h);
        }
        gen_random_tex_data(w * h, tex);
    }

    if (rank == 0) {
        assert(tex);
        assert(out);
    }

#ifdef ENABLE_GUI
    visual_window win;
    win.width = 1280;
    win.height = 720;

    if (rank == 0 && opts.use_gui) {
        ret = create_window(&win);
        if (ret != 0) {
            log_error("Failed to create window!");
            return 1;
        }
        ret = render_texture(&win, tex, TARGET_RAW, w, h, w * 3);
        if (ret != 0) {
            log_error("Failed to render texture data!");
            return 1;
        }
    }
#endif

    struct timespec s, e;
    clock_gettime(CLOCK_MONOTONIC, &s);

    if (rank == 0) {
        meta[META_PITCH] = 3;
        meta[META_STRIDE] = w * meta[META_PITCH];
        meta[META_BLK_W] = bw;
        meta[META_BLK_H] = bh;
        meta[META_DISPATCH] = opts.batch_size ? bh * opts.batch_size : bh;
        meta[META_WIDTH] = w;
        meta[META_HEIGHT] = h;
        meta[META_FLAGS] = (opts.trace_steps ? ENC_FLAG_VERBOSE : 0) |
                           (opts.test_run ? ENC_FLAG_INTERMEDIATE : 0) |
                           (opts.partitioning ? ENC_FLAG_PARTITIONING : 0) |
                           (opts.threads != 1 ? ENC_FLAG_THREADING : 0);
        meta[META_MAX_THR] = opts.threads;
    }

    struct local_worker_opts wopts;
    pthread_t worker_thread;

#ifdef ENABLE_MPI
    if (opts.use_mpi) {
        if (rank == 0) {
            int nt = 1;
            if (opts.use_mpi == 1) {
#pragma omp parallel
                {
                    if (omp_get_thread_num() == 0)
                        nt = omp_get_num_threads();
                }
                wopts.counter.store(0, std::memory_order_relaxed);
                wopts.opts = &opts;
                wopts.meta = meta;
                pthread_create(&worker_thread, NULL, worker_local, &wopts);
                /* Set threads to n - 1, since thread 0 should be the controller
                    * thread */
                controller((void *)tex, (void *)out, &wopts, meta, rank, size,
                        nt - 1);
            }
        } else {
            worker(&opts, rank, size);
        }
    } else {
#endif
        uint8_t * tex_tmp = (uint8_t *)tex;
        uint8_t * out_tmp = out;
        int num_batches = meta[META_HEIGHT] / meta[META_DISPATCH];
        int comp_batch_size;
        if (opts.test_run) {
            comp_batch_size = meta[META_DISPATCH] * meta[META_STRIDE];
        } else {
            comp_batch_size =
                get_comp_size_astc(meta[META_WIDTH], meta[META_DISPATCH],
                                   meta[META_BLK_W], meta[META_BLK_H]);
        }
        float comp_ratio =
            meta[META_STRIDE] * meta[META_DISPATCH] / comp_batch_size;
        log_debug(
            "Batch count: %d, uncompressed %d bytes, compressed %d bytes (%d "
            "ASTC blocks), ratio: %.2f:1",
            num_batches, meta[META_STRIDE] * meta[META_DISPATCH],
            comp_batch_size, comp_batch_size / 16, comp_ratio);
        for (; num_batches; --num_batches) {
            // log_debug("left: %d, src: %p, dst: %p", num_batches, (void
            // *)tex_tmp, (void *)out_tmp);
            encode_blocks(meta, (void *)tex_tmp, (void *)out_tmp, opts.threads);
            tex_tmp += meta[META_DISPATCH] * meta[META_STRIDE];
            out_tmp += comp_batch_size;
        }
#ifdef ENABLE_MPI
    }
#endif

    int output_flag =
        opts.out_filename && (!opts.use_mpi || (opts.use_mpi && rank == 0));
    if (output_flag) {
        if (opts.test_run) {
            stbi_write_png(opts.out_filename, w, h, 3, (void *)out, w * 3);
        } else {
            FILE * f = fopen(opts.out_filename, "w");
            if (!f) {
                log_error("Failed to open file %s for writing",
                          opts.out_filename);
                log_error("Texture will not be saved");
            } else {
                if (!opts.raw_output) {
                    struct astc_header hdr;
                    hdr.magic[0] = 0x13;
                    hdr.magic[1] = 0xAB;
                    hdr.magic[2] = 0xA1;
                    hdr.magic[3] = 0x5C;
                    hdr.block_x = bw;
                    hdr.block_y = bh;
                    hdr.block_z = 1;
                    hdr.dim_x[0] = (uint8_t)(w & 0xFF);
                    hdr.dim_x[1] = (uint8_t)((w >> 8) & 0xFF);
                    hdr.dim_x[2] = (uint8_t)((w >> 16) & 0xFF);
                    hdr.dim_y[0] = (uint8_t)(h & 0xFF);
                    hdr.dim_y[1] = (uint8_t)((h >> 8) & 0xFF);
                    hdr.dim_y[2] = (uint8_t)((h >> 16) & 0xFF);
                    hdr.dim_z[0] = 1;
                    hdr.dim_z[1] = 0;
                    hdr.dim_z[2] = 0;
                    size_t written = fwrite(&hdr, sizeof(hdr), 1, f);
                    if (written != 1) {
                        log_error("Writing failed");
                    }
                }
                size_t written = fwrite(out, w * h, 1, f);
                if (written != 1) {
                    log_error("Writing failed %lu != %lu", written,
                              (size_t)(w * h));
                }
                fclose(f);
            }
        }
    }

    /* free resources */
    free(tex);
    free(out);

    clock_gettime(CLOCK_MONOTONIC, &e);
    float t = calc_seconds(&s, &e);
    char u;
    float v = conv_unit(w * h / t, &u);

#ifdef ENABLE_MPI
    if (opts.use_mpi) {
        if (rank == 0) {
            log_info("Done in %.3f ms, rate: %.3f %cpix/s", t * 1000, v, u);
        }
    }

    if (opts.use_mpi) {
        if (rank == 0) {
            void * t_ret;
            pthread_join(worker_thread, &t_ret);
            if ((uintptr_t)t_ret != 0) {
                log_error("Local worker thread failed: %s", strerror(ret));
            }
        }
        finalize_mpi();
    }
#endif

    if (opts.bench) {
        if (rank == 0)
            printf("%f,%f\n", t * (float)1000, w * h / t);
    } else if (!opts.use_mpi) {
        log_info("Done in %.3f ms, rate: %.3f %cpix/s", t * 1000, v, u);
    }

#ifdef ENABLE_GUI
    if (rank == 0 && opts.use_gui)
        pthread_join(win.thread, NULL);
#endif

    return 0;
}