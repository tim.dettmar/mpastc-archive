#ifndef MPASTC_WORKER_H_
#define MPASTC_WORKER_H_

#include "app_encode.h"
#include "convert.h"
#include "log.h"
#include "mpiutil.h"
#include "worker_opts.h"

#include <assert.h>
#include <atomic>
#include <errno.h>
#include <mpi.h>
#include <omp.h>
#include <stdint.h>
#include <stdio.h>

int worker(struct mpa_options * opts, int rank, int size);

void * worker_local(void * worker_opts);

#endif