/* Generates random texture data for testing */

#include "texel.h"
#include <stdint.h>
#include <stdlib.h>

/* Generates uniformly random texel data. */
static void gen_random_tex_data(uint64_t n, texel * out) {
    for (uint64_t i = 0; i < n; i++) {
        out[i].r = (uint8_t)(random() % 128);
        out[i].g = (uint8_t)(random() % 128);
        out[i].b = (uint8_t)(random() % 128);
    }
}

/* Generates only different shades of a single random base colour */
static void gen_grayscale_data(uint64_t n, texel * out) {
    texel base;
    base.r = (uint8_t)(random() % 256);
    base.g = (uint8_t)(random() % 256);
    base.b = (uint8_t)(random() % 256);
    for (uint64_t i = 0; i < n; i++) {
        out[i].r = (uint8_t)(((float) base.r) * (random() / RAND_MAX));
        out[i].g = (uint8_t)(((float) base.g) * (random() / RAND_MAX));
        out[i].b = (uint8_t)(((float) base.b) * (random() / RAND_MAX));
    }
}