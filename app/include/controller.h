#ifndef MPASTC_CONTROLLER_H_
#define MPASTC_CONTROLLER_H_

#include "log.h"
#include "mpiutil.h"
#include "worker_opts.h"
#include "convert.h"

#include <errno.h>
#include <mpi.h>
#include <omp.h>
#include <stdint.h>
#include <stdio.h>
#include <assert.h>

int controller(void * buf, void * out, struct local_worker_opts * ctrl,
               uint32_t meta[META_MAX], int rank, int worldsize, int nthreads);

#endif