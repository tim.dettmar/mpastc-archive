#include <pthread.h>

static inline int q_lock_init(pthread_mutex_t * lock) {
    return pthread_mutex_init(lock, NULL);
}

static inline int q_lock(pthread_mutex_t * lock) {
    return pthread_mutex_lock(lock);
}

static inline int q_unlock(pthread_mutex_t * lock) {
    return pthread_mutex_unlock(lock);
}

static inline int q_trylock(pthread_mutex_t * lock) {
    return pthread_mutex_trylock(lock);
}