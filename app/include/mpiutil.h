#ifndef MPASTC_MPIUTIL_H_
#define MPASTC_MPIUTIL_H_

#include <mpi.h>
#include <omp.h>

#include "format.h"
#include "log.h"
#include <errno.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

typedef enum {
    MPA_TAG_NONE,
    MPA_TAG_META,
    MPA_TAG_CTRL,
    MPA_TAG_DATA_A,
    MPA_TAG_DATA_B,
    MPA_TAG_MAX
} mpa_tag;

#define MPA_META_TYPE MPI_UINT32_T
#define CONV_ARR_LEN (sizeof(conv_info) / sizeof(uint32_t))

int get_thread_count();

int init_mpi(int * rank, int * size);

int finalize_mpi();

static inline uint8_t * get_row_data(uint8_t * texels, uint32_t stride,
                                     uint32_t dh, uint32_t index) {
    return texels + stride * dh * index;
}

int32_t max_value(int32_t * vals, size_t len);

int32_t min_completed(int32_t * vals, size_t len);

#endif