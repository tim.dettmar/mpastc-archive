#include "texel.h"
#include <errno.h>
#include <stdint.h>

#define LOCK_ATOMIC 0
#define LOCK_PTHREAD 1
#define LOCK_TYPE LOCK_PTHREAD

#if LOCK_TYPE == LOCK_ATOMIC
#include "lock_atomic.h"
#define Q_LOCK atomic_flag
#elif LOCK_TYPE == LOCK_PTHREAD
#include "lock_pthread.h"
#define Q_LOCK pthread_mutex_t
#else
#error "Lock type must be 'atomic' or 'pthread'"
#endif

#define PART_FLAG_1 (1)      // Single partition
#define PART_FLAG_2 (1 << 1) // 2
#define PART_FLAG_3 (1 << 2) // 3
#define PART_FLAG_4 (1 << 3) // 4-partition

enum control_flags {
    CTRL_INVALID = 0,
    /* Calculate the best endpoints */
    CTRL_ENDPOINTS = 1UL,
    /* Calculate the best weights */
    CTRL_WEIGHTS = 1UL << 1,
    /* Determine the optimal quantization parameters */
    CTRL_QUANT = 1UL << 2,
    /* Perform k-means clustering (partition only) */
    CTRL_KMEANS = 1UL << 3,
    /* Instead of k-means, perform an exhaustive endpoint/weight search on
       every provided partition within the specified range. Very slow! */
    CTRL_EXHAUSTIVE = 1UL << 4,
    /* Pads the enum to force uint64_t type for flags */
    CTRL_PAD_UINT64 = 1UL << 63
};

enum comp_status {
    COMP_STAT_NONE,            // No operations performed on raw data yet
    COMP_STAT_FOUND_ENDPOINTS, // Colour endpoints already found
    COMP_STAT_MAX
};

struct tex_info {
    uint32_t width;  // Image width
    uint32_t height; // Image height
    uint8_t bw;      // Block width (if applicable)
    uint8_t bh;      // Block height (if applicable)
    uint32_t pitch;  // Bits per pixel
    uint32_t stride; // Bytes per row, including padding
    void * base;     // Start of the texture data
};

struct tex_scratch {
    ir_endpoint endpoints_1;    // Endpoint colours for 1-partition mode
    ir_endpoint endpoints_2[2]; // Endpoint colours for 2-partition mode
    ir_endpoint endpoints_3[3]; // Endpoint colours for 3-partition mode
    ir_endpoint endpoints_4[4]; // Endpoint colours for 4-partition mode
    /* Weights are variable-length, based on the current block size */
    ir_weight * weights_1; // Weights for 1-partition mode
    ir_weight * weights_2; // Weights for 2-partition mode
    ir_weight * weights_3; // Weights for 3-partition mode
    ir_weight * weights_4; // Weights for 4-partition mode
    /* Centroids are used during k-means, but are not necessary for encoding */
    ir_texel centroids_2[2];
    ir_texel centroids_3[3];
    ir_texel centroids_4[4];
    /* Best quant level. Contents treated as read-only array [color, weight]
     * quant level*/
    uint8_t * best_quant;
    /* Best MSE search result */
    float * mse;
};

struct comp_queue {
    Q_LOCK * lock;            // Queue lock
    struct tex_info * in;     // Input texture info
    struct tex_info * out;    // Output texture info
    struct tex_scratch * scr; // Temporary information
    uint64_t control;         // Control flags
    uint32_t start_id;        // Starting block ID to process (inclusive)
    uint32_t end_id;          // Ending block ID to process (inclusive)
    uint16_t start_pid;       // Starting partition ID to process (inclusive)
    uint16_t end_pid;         // Ending partition ID to process (inclusive)
    uint8_t parts;            // Partition test flags (PART_FLAG_*)
};