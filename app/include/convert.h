#ifndef MPASTC_APP_CONVERT_H_
#define MPASTC_APP_CONVERT_H_

typedef enum {
    META_WIDTH,      // Image width
    META_HEIGHT,     // Image height
    META_PITCH,      // Pixel pitch
    META_STRIDE,     // Row stride incl. padding
    META_DISPATCH,   // Dispatch row count (min number: block height)
    META_IN_FORMAT,  // Input format
    META_OUT_FORMAT, // Output format
    META_BLK_W,      // Block width
    META_BLK_H,      // Block height
    META_FLAGS,      // Control flags
    META_MAX_THR,    // Maximum threads
    META_MAX         // Sentinel value
} mpa_conv_info;

static inline int get_comp_size_astc(int w, int h, int bw, int bh) {
    return (w / bw) * (h / bh) * 16;
}

#endif