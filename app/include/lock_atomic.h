#include <stdatomic.h>

static inline int q_lock_init(atomic_flag * lock) {
    atomic_flag_clear_explicit(lock, memory_order_release);
    return 0;
}

static inline int q_lock(atomic_flag * lock) {
    while (atomic_flag_test_and_set_explicit(lock, memory_order_acquire));
    return 0;
}

static inline int q_unlock(atomic_flag * lock) {
    atomic_flag_clear_explicit(lock, memory_order_release);
}

static inline int q_trylock(atomic_flag * lock) {
    return atomic_flag_test_and_set_explicit(lock, memory_order_acquire);
}