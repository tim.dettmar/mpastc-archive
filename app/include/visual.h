#ifndef MPASTC_VISUAL_H_
#define MPASTC_VISUAL_H_

#include <GLFW/glfw3.h>
#include <atomic>
#include <stdint.h>
#include <stdio.h>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"

#include <pthread.h>

#define VIS_SHOW_RAW_TEX 1

typedef enum {
    TARGET_RAW = 0,
    TARGET_COMP
} visual_target;

typedef struct {
    void * buf;
    uint32_t width;
    uint32_t height;
    GLint format;
    visual_target target;
} texture_render_cmd;

typedef struct {
    GLFWwindow * window;
    ImGuiIO io;
    pthread_t thread;
    pthread_mutex_t mut;
    uint32_t width;
    uint32_t height;
    uint64_t flags;

    uint32_t tex_w;
    uint32_t tex_h;
    GLuint tex_ids[2];

    texture_render_cmd * cmd;
} visual_window;


static texture_render_cmd cmd;

int create_window(visual_window * out);

int render_texture(visual_window * ctx, void * tex, uint8_t type,
                   uint32_t width, uint32_t height, uint32_t stride);

#endif