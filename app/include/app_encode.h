#ifndef MPASTC_APP_ENCODE_H_
#define MPASTC_APP_ENCODE_H_

#include "astcenc_interop.h"

#ifdef ENABLE_MPI
#include "mpiutil.h"
#endif

enum app_encode_flags {
    ENC_FLAG_NONE,
    ENC_FLAG_VERBOSE = 1,
    ENC_FLAG_INTERMEDIATE = 2,
    ENC_FLAG_PARTITIONING = 4,
    ENC_FLAG_THREADING = 8,
};

#include "bitwriter.h"
#include "block.h"
#include "convert.h"
#include "enccomb.h"
#include "encode.h"
#include "kmeans.h"
#include "log.h"
#include "part.h"
#include "partition.h"
#include "pca.h"
#include "texel.h"

#include <math.h>
#include <omp.h>
#include <stdint.h>

struct best_encoding {
    ir_endpoint eps[4];
    ir_weight wts[16];
    uint8_t ranges[2];
    ir_fmt err;
    uint8_t part_count;
    uint16_t part_id;
};

// void place_test_block(texel * src, texel * out, uint32_t id, uint8_t bw,
//                       uint8_t bh, uint32_t width, uint32_t height);

void unpack_test_blocks(texel * texels, texel * out, uint8_t bw, uint8_t bh,
                        uint32_t width, uint32_t height);

int encode_blocks(uint32_t meta[META_MAX], void * buf, void * out,
                  int n_threads);

#endif