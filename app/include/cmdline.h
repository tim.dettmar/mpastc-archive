#ifndef MPASTC_CMDLINE_H_
#define MPASTC_CMDLINE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <errno.h>
#include <getopt.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct mpa_options {
    uint8_t use_mpi;      // Enable MPI
    uint8_t use_gui;      // Enable GUI
    uint8_t trace_steps;  // Trace calculation steps
    uint8_t partitioning; // Partitioning mode
    uint8_t bench;        // Benchmark mode
    uint8_t test_run;     // Dry run - test run
    uint32_t threads;     // Number of threads to use
    uint32_t batch_size;  // Dispatch multiplier
    char * filename;      // Input file
    char * out_filename;  // Output file
    uint8_t raw_output;   // Raw output requested
    uint8_t help_req;     // Help
};

void print_help();

int load_opts(int argc, char ** argv, struct mpa_options * out);

#ifdef __cplusplus
}
#endif

#endif