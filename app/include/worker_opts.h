#ifndef MPASTC_WORKER_OPTS_H_
#define MPASTC_WORKER_OPTS_H_

#include "cmdline.h"

#include <atomic>

struct local_worker_opts {
    struct mpa_options * opts;    // Global options
    std::atomic<int64_t> counter; // Progress counter / lock
    uint32_t * meta;              // Metadata
    void * in_buf;                // Raw texel buffer
    void * out_buf;               // Output buffer
    int nthreads;                 // Number of threads to use
};

#endif