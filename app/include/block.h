#ifndef MPASTC_BLOCK_H_
#define MPASTC_BLOCK_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "texel.h"
#include <errno.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

/*  Extract the pixel data from raw texel data.
    raw:     Raw input data in RGB888 format
    out:     Output data
    convert: Whether to convert the texel data into the intermediate format or
             leave it as is (uint8_t)
    stride:  Stride in bytes of the row including padding
    x:       Starting pixel of the block (horizontal)
    y:       Starting pixel of the block (vertical)
    bw:      Block width
    bh:      Block height
*/
void extract_block(uint8_t * raw, void * out, uint8_t convert, uint64_t stride,
                   int64_t x, int64_t y, uint8_t bw, uint8_t bh);

/*  Convert a block ID (stored in row-major order) to the x,y coordinates
 *  (starting from the top-left) of the first pixel of the corresponding block
 *  id: Block ID
 *  w: Image width
 *  bw: Block width
 *  bh: Block height
 *  x: Output x coordinate
 *  y: Output y coordinate
 */
static inline void id_to_index(int id, uint32_t w, uint8_t bw, uint8_t bh,
                               uint32_t * x, uint32_t * y) {
    *x = id % (w / bw) * bw;
    *y = id / (w / bw) * bh;
}

#ifdef __cplusplus
}
#endif

#endif