# mpASTC

Multiprocessor, multi-node ASTC compressor

mpASTC is a parallel texture compressor for the Adaptive Scalable Texture
Compression (ASTC) format.

The goal of mpASTC is to produce high-quality compressed textures in real-time
through parallelism.

## Folder Structure

`mpastc/`:        The mpASTC library  
`mpastc/codegen`: Code generator and automatically-generated header files  
`app/`:           Encoding application  

## Building

You need the following packages to build mpASTC:

- C & C++ compiler supporting C11 and C++11, with OpenMP support
- CMake 3.9.0+
- GNU make 4.0+
- An MPI 3.0+ implementation (app only, if MPI support is enabled)

Check your distribution or cluster configuration for where these can be found.

You must generate partition and encoding combination code **before** compiling
mpASTC. For size reasons this is not included in the repository.

Go to the folder `mpastc/codegen` and run `make`. This will generate the header
files used by the mpASTC library. Verify the header files `enccomb.h` and
`partition.h` have been created.

If you want to compile the mpASTC **user-facing** application, you can do this
after code-generation by changing into the `app/` directory. Simply run the
command `cmake .` \- you can define the following as well with the format
`-DOPTION=1`:

- `ENABLE_MPI`: Enable MPI work distribution support.
- `ENABLE_DEBUG`: Enable debug asserts and flags
- `ENABLE_SAN`: Enable address & undefined behaviour sanitizers

While CMake might claim the variables are unused, you will see output such as 
`MPI support enabled` if you did this correctly.

Should you wish to use the mpASTC **library** in your own program, you only need
to compile the mpastc application in the `mpastc/` folder. Either compile and
copy the `.a` file and headers into your own program, or, if you use CMake, add
the `mpastc` folder as a subdirectory and link against `mpastc`.


## License

Copyright (c) 2023 Tim Dettmar

mpASTC incorporates the following third-party software:

- Headers from the [ARM reference ASTC encoder](https://github.com/ARM-software/astc-encoder), Apache 2.0 License
- log.c logging library: [link](https://github.com/rxi/log.c), MIT License
- stb_image from the stb libraries: [link](https://github.com/nothings/stb), MIT License

mpASTC is licensed under the BSD 3-Clause License. See the file LICENSE for
details.
